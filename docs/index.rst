.. DW Common Modules for Lambda documentation master file, created by
   sphinx-quickstart on Sun Jan 28 17:23:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation for DW Common Modules for Lambda!
==============================================================

This library consists of several submodules.
Some of them use additional dependencies
which can be pulled in with ``extras`` when installing the package:

.. code-block:: bash

    # from local repository
    pip install kasasa_lambda[xray,tests]
    # or from git
    pip install git+ssh://git@gitlab.com/balajin/dw-common-lambda#egg=kasasa_lambda[xray,tests]

In this example we requested extras for `.xray` and `tests <.tests.__main__>` submodules.
Not all modules have their extras; some modules use only core dependencies like `boto3`,
they don't provide extras -- at least for now.
For other modules, extra name corresponds to submodule name.
Please refer to ``setup.py`` for the list of available extras.

When the `kasasa_lambda` module (or any of its submodules) is imported,
it will by default configure root logger to ``INFO`` loglevel.
That is to make sure all lambda functions will by default log verbose enough.
This behaviour can be disabled by setting :envvar:`NO_FORCE_LOGGING` environment variable
to non-empty string;
it may be useful e.g. for testing, when excess logging is not convenient.

.. envvar:: NO_FORCE_LOGGING

   Don't set default logger's loglevel to `INFO` on module importing
   but leave it with the default value of ``WARNING``.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   aws
   others
   standalone


TODO items
==========

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
