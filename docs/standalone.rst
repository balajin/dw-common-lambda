Standalone utilities
====================

MakeZip: yet another lambda deployment helper
---------------------------------------------

.. automodule:: kasasa_lambda.makezip


Running Tests for the library
-----------------------------

.. automodule:: kasasa_lambda.tests.__main__
