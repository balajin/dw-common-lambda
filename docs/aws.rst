AWS Helpers
===========

These modules provide helper classes which simplify using certain AWS services.
They are mostly based on `boto3` library but have simpler object-oriented interface.

S3
--

.. automodule:: kasasa_lambda.s3

SQS
---

.. automodule:: kasasa_lambda.sqs

SNS
---

.. automodule:: kasasa_lambda.sns

Lambda
----------

.. automodule:: kasasa_lambda.alambda

KMS
---

.. automodule:: kasasa_lambda.kms

X-Ray
---------

.. automodule:: kasasa_lambda.xray
