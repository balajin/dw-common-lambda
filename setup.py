import sys
import setuptools

# We cannot use environment markers for this,
# becasue we may be running with setuptools==16.x which is bundled with lambda
# while that feature is properly supported since about 20.x.
# But we still require new setuptools for makezip to work properly.
add_req = []
if sys.version_info < (3, 4):
    add_req += [
        'backports.tempfile',
        'backports.csv',

        # looks like it is missing from lambda/py2.7
        # but its pkg_resources is required for e.g. aws-xray-sdk
        'setuptools>=36',
    ]

setuptools.setup(
    name='kasasa_lambda',
    version='1.15.0',
    description='Common code for DW lambda functions',
    url='https://gitlab.com/balajin/dw-common-lambda',
    classifiers=[
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    packages=['kasasa_lambda', 'kasasa_lambda.tests'],
    package_data={
        # we want to keep any files placed to tests dir
        # as they are used for tests.
        # Some of them are .pig scripts
        # or keyfiles.
        # FIXME this also includes all __pycache__ stuff which we should avoid
        'kasasa_lambda.tests': ['*', '*/*', '*/*/*'],
    },
    install_requires=[
        # FIXME remove this after https://github.com/spulec/moto/issues/1793
        # will be fixed:
        'boto3<1.8',
        'botocore<1.11',
        'aws-xray-sdk<0.96',
        # Without this initial pinning (before other deps)
        # things go wrong.

        'python-dotenv',  # for loading env vars from file if one exists
        'boto3',
        # we are on AWS lambda, so will most likely want to access
        # other AWS services like S3 or KMS - hence boto3

        # for compatibility with different python versions:
        'six',
    ] + add_req,
    extras_require={
        'tests': [
            'pytest',
            'pytest-cov',
            'pytest-mock',
            'pytest-yamltree',
            'moto>=1.1.25',  # 1.1.23 for xray support
        ],
        'db': ['pymysql'],
        'pig': ['sh>=0.12'],
        's3': ['contextlib2'],
        'crypt': [
            # TODO migrate to plain `gnupg` which is newer
            # after https://github.com/isislovecruft/python-gnupg/issues/205
            # is fixed
            'python-gnupg',
        ],
        'makezip': [
            'pip',
            'setuptools>=36',  # just require new enough for --home to work
        ],
        'skel': [
            'python-dateutil',  # for timestamp parsing
        ],
        'xray': [
            'aws-xray-sdk',
            'wrapt',
            # wrapt is actually already requested by aws-xray-sdk
            # but let's be sure
            'contextlib2',
        ],
        # XXX if you add an extra,
        # XXX don't forget to mention it in requirements.txt!
        # TODO add "everything" extra instead?
    },
    entry_points={
        'console_scripts': [
            'makezip=kasasa_lambda.makezip:main [makezip]',
        ],
    },
)
