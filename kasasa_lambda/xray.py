"""
This module wraps `aws_xray_sdk`
and provides several improvements over it:

* :func:`capture` can instrument iterator steps and does it by default
* :func:`capture` tries to properly handle instance methods' names -
  when decorating ``MyClass.meth`` it will name subsegment ``MyClass.meth``
  and not just ``meth``, unlike the original version
* :func:`capture` won't try to log subsegments
  until activated with :func:`activate_capture` (or :func:`activate`).
  This makes it possible to use :func:`capture` in libraries
  without logging any failures when used in non-instrumented environment.
* Added :func:`within_segment` helper
  to easily wrap non-Lambda code in a segment
* Log entries are saved to the metadata_ of current segment or subsegment,
  and if `dict`-based value substitution is used
  (``logger.info('Processed %(count)d items', dict(count=3))``)
  then will also save that dict's values to annotations_.

.. _metadata: https://docs.aws.amazon.com/xray/latest/devguide/xray-concepts.html#xray-concepts-annotations
.. _annotations: https://docs.aws.amazon.com/xray/latest/devguide/xray-concepts.html#xray-concepts-annotations

Use it like this::

    from kasasa_lambda import xray

    # before using the helper you will want to activate it;
    # that includes patching supported libraries
    # and enabling log capture helper.
    xray.activate()
    # If you don't want to capture logs
    # then use this:
    #xray.activate(capture_logs=False)

    # capture can be used without an argument
    @xray.capture
    def func():
        pass

    # or you can override function name
    @xray.capture('awesome-func')
    def func2():
        pass

    # you can also refer to the original
    # aws_xray_sdk.core.xray_recorder instance
    xray.xray_recorder.start_segment()
    ...


.. autofunction:: patch_all

   .. aws_xray_sdk's function has no docstring, so describe it here

   Patch all supported libraries
   to automatically log requests to xray.

   Supported libraries include `boto3`, `requests`, `sqlite3` and `mysql`.
   Refer to `AWS docs`__ for details.

   You will most likely don't use this function directly
   and use :func:`activate` instead.

   __ https://docs.aws.amazon.com/xray/latest/devguide/xray-sdk-python-patching.html
"""

import inspect
import itertools
import json
import logging
import time
import traceback
import warnings

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all
from aws_xray_sdk.core.exceptions.exceptions import SegmentNotFoundException
import contextlib2
import six
import wrapt  # it's used by aws_xray_sdk hence no need to require?


logger = logging.getLogger(__name__)


def activate(capture_logs=True):
    """
    Activate XRay capture.
    This involves patching supported libraries with :func:`patch_all`
    and logging configuration (:func:`catch_logs`).

    :param bool capture_logs: whether you want to save logs
        to subsegment metadata.
        You may want to disable it
        if you are going to run a long-running process
        in which case you could bump into the hidden limit:
        https://forums.aws.amazon.com/thread.jspa?threadID=262253
    """
    patch_all()
    if capture_logs:
        catch_logs()
    patch_context()
    activate_capture()


capture_is_active = False

def activate_capture():
    """
    By default, :func:`capture` is inactive,
    meaning functions it wraps won't try to log anything.
    This is to avoid warnings in environment not using XRay.
    After you call :func:`activate_capture`,
    any :func:`capture`-wrapped functions will start logging subsegments.
    """
    global capture_is_active
    capture_is_active = True

    # Default value is 30, but with our logging approach it is often too large.
    # So let's decrease it.
    # https://github.com/aws/aws-xray-sdk-python/issues/21
    # TODO either decrease dynamically or do something better?..
    xray_recorder.streaming_threshold = 15


def patch_context():
    from aws_xray_sdk.core.context import Context

    # double patching will break things! Avoid it!
    if hasattr(Context, 'reset_counters'):
        raise ValueError('Context is already patched')

    # We cannot know which context object will be actually used,
    # be it regular Context, LambdaContext or something special.
    # So we monkey-patch base Context class.

    # TODO patch context, not segment.
    # Store active loglevel and global counter in self._local,
    # because they will be cleared when segment is changed (TODO check that).
    # When throttling should occur because of counter,
    # probably handle it on context level, not on segment level
    # (TODO check if that could leave some consequences like segment counters)
    #
    # XXX _local is not always cleared. We need to take care.
    # Override end_segment() and clear counters in it

    def reset_counters(self):
        self._local.loglevel = 0
        self._local.total_subsegments = 0
        self._local.current_subsegemnt = 0

    Context.reset_counters = reset_counters

    def wrapper_reset_counters(wrapped, self, args, kwargs):
        ret = wrapped(*args, **kwargs)
        self.reset_counters()
        return ret

    # when ending segment or on any _local reset,
    # set counters to initial values
    # FIXME __init__ patching? it has no meaning
    # at least because context instance is already created when we patch.
    for name in ('__init__', 'end_segment', 'clear_trace_entities'):
        wrapt.wrap_function_wrapper(Context, name, wrapper_reset_counters)

    @wrapt.patch_function_wrapper(Context, 'put_subsegment')
    def wrap_put_subsegment(wrapped, self, args, kwargs):
        wrapped(*args, **kwargs)
        # FIXME we cannot know if it (^^) was successful
        # vv: sometimes total_subsegments is empty
        # (because __init__ cannot be correctly overriden)
        # so we need getattr
        self._local.total_subsegments = getattr(
            self._local, 'total_subsegments', 0) + 1

    def check_subsegment_unsampling(self):
        """
        Check if current subsegment should be really added.
        This should be called by the subsegment decorator function
        whenever it thinks of adding a subsegment
        (but after checking for loglevel).

        This function implements simple algorithm.
        We know that we are not allowed more than 5000 subsegments per segment;
        so after we exceed 1/2 of that value (2500),
        we accept only every second subsegment;
        after we exceed 3/4 of the limit (3750),
        we accept only every 4th subsegment; and so on.
        That way, we can guarantee we will never overflow the limit
        and at the same time we try our best to make partially representative
        image of what happens.
        """
        # increment tested subsegment counter
        # (we need getattr as we could not properly patch __init__)
        sub_id = getattr(self._local, 'current_subsegment', 0)
        sub_id += 1
        self._local.current_subsegment = sub_id

        # this is total number of *already added* subsegments,
        # it is incremented in put_subsegment wrapper.
        total = getattr(self._local, 'total_subsegments', 0)

        # test against thresholds
        max_subs = 5000  # maximum allowed number of subsegment per segment
        if total >= max_subs:
            logger.info('Cannot add subsegment because max count is over')
            return False

        # max_subs - rest = current threshold
        rest = max_subs
        every = 1
        while rest >= 1:
            rest /= 2.0
            if total > max_subs - rest:
                # this threshold is exceeded, so double our skip rate
                every *= 2
        if sub_id % every == 0:
            return True
        logger.info('Skipping segment #%d: total %d is beyond threshold',
                    sub_id, total)
        return False

    Context.check_subsegment_unsampling = check_subsegment_unsampling


class XRayLogHandler(logging.Handler):
    # If we put too many log entries in a single subsegment
    # then we will have problems when trying to pickle that subsegment
    # and won't be able to send it.
    # So let's limit max log entries count.
    # TODO limit by bytes count, not by entries count
    # TODO: make this setting better configurable from outside
    MAX_ENTRIES_PER_SUBSEGMENT = 100

    def filter(self, record):
        # There is a problem:
        # in `.emit` we call `xray_recorder.get_trace_entity`;
        # but when there is no trace entity, it will in turn
        # log an error, resulting in one another log entry,
        # which will result in subsequent call to `.emit` etc.
        # In order to avoid this recorsion,
        # we will filter out log records coming from aws_xray_sdk.

        # We could use `logging`'s "built-in" mechanism of `Filter`s,
        # but this approach (overriding filter meth) is simpler in our case.

        if record.name.startswith('aws_xray_sdk.core'):
            return False
        return super(XRayLogHandler, self).filter(record)

    def ensure_json(self, value):
        if isinstance(value, six.string_types + six.integer_types + (
            float, bool,
        )):
            return value
        elif isinstance(value, dict):
            return {k: self.ensure_json(v) for k, v in value.items()}
        elif isinstance(value, (list, tuple)):
            return [self.ensure_json(v) for v in value]
        # all other types shall be stringified,
        # or else it might cause problems when jsonpickle'ing data -
        # mostly because of recursive references.
        return str(value)

    def emit(self, record):
        # get current segment or subsegment
        try:
            entity = xray_recorder.get_trace_entity()
        except SegmentNotFoundException:
            # FIXME would be great to determine segment existence in advance,
            # witout causing this exception (probably)
            # and log error (always).
            # In current implementation, when there is no active segment,
            # every "normal" log entry will be always accompanied by an error.
            return
        if not entity:
            # no current entity -> nothing to do.
            # XRay already logged a warning from its get_trace_entity
            # so we don't need to log anything here.
            return

        # format data
        data = record.args
        if isinstance(data, dict):  # FIXME better use collections.Mapping
            # dict-based formatting, we can store fields
            annotate = data
            # create a copy and modify it safely
            data = dict(data)
        else:
            # tuple-based fomatting
            data = {'args': data}
            # nothing to annotate
            annotate = {}
        data['tpl'] = record.msg
        data['msg'] = record.getMessage()

        data = self.ensure_json(data)

        # and store data in the entity's metadata
        # XXX: here we bypass Entity.put_metadata
        # because it doesn't support appending to list.
        # XXX: maybe better store a tuple, not list?
        # how will it work with mutable type?
        # namely when saving partial segment etc.
        entity.metadata.setdefault('default', {})  # make sure namespace exists
        entity.metadata['default'].setdefault('logs', []) # and logs list
        logs = entity.metadata['default']['logs']
        if self.MAX_ENTRIES_PER_SUBSEGMENT is None:
            logs.append(data)
        elif len(logs) < self.MAX_ENTRIES_PER_SUBSEGMENT:
            logs.append(data)
        elif len(logs) == self.MAX_ENTRIES_PER_SUBSEGMENT:
            logs.append({
                'msg': '*** max logs count exceeded ***',
                'count': self.MAX_ENTRIES_PER_SUBSEGMENT,
            })

        # if we had dict-based args then also store them as annotations
        # this will probably allow to search subsegments with e.g. zero records
        for key, val in annotate.items():
            # FIXME does not work?
            if isinstance(val, six.string_types):
                # convert unicodes to strings
                val = str(val)
            elif not isinstance(val, six.integer_types + (float, bool)):
                # this won't work and will yield warning,
                # so don't do it
                continue
            # XXX it seems that, despite not mentioned in docs,
            # annotation name cannot include dot or colon -
            # only alphanumeric and underscore are allowed.
            # Otherwise annotation entry is just silently ignored.
            # See issue aws/aws-xray-sdk-python#18
            entity.put_annotation('%s' % key, val)
        # and set flag for errors and warnings
        if record.levelname == 'WARNING':
            entity.put_annotation('log_warning', True)
        elif record.levelno >= logging.ERROR:
            entity.put_annotation('log_error', True)


def catch_logs():
    """
    Capture all log entries which don't exceed the root logger's loglevel
    and store them to metadata of current xray entity.

    You will most likely don't use this function directly
    and use :func:`activate` instead.
    """
    root = logging.getLogger()
    root.addHandler(XRayLogHandler())


def _iter_wrapper(iterator, name):
    """
    This function is a generator function intended to wrap an iterator.
    It will log a subsegment for each iterator step
    and handle exceptions correctly.
    """
    seg = None
    try:
        for i in itertools.count(start=0):
            seg = xray_recorder.begin_subsegment(
                '{}-iter-{}'.format(name, i), 'local')

            # fetch value
            val = next(iterator)
            # remember when fetching finished
            end_time = time.time()

            # We could attach val to seg as metadata,
            # but that might lead to sensitive information leakage.
            # So we don't implement it.

            xray_recorder.end_subsegment(end_time)
            seg = None

            yield val

    except Exception as e:
        if seg:  # exception not in "yield" clause
            end_time = time.time()
            if isinstance(e, StopIteration):
                seg.put_metadata('final', True)
            else:
                # not a normal StopIteration so record traceback
                seg.add_exception(e, traceback.extract_stack())
            xray_recorder.end_subsegment(end_time)
        raise


def _wrap_iter(fn, args, kwargs, name):
    """
    Wrap given function's result with :func:`_iter_wrapper`
    if and only if it itself is a generator function.
    """
    ret = fn(*args, **kwargs)
    if not inspect.isgenerator(ret):
        return ret

    # it is iterator, so we want to return iterator as well
    return _iter_wrapper(ret, name)


default_loglevel = 0


def check_loglevel(level):
    segment = xray_recorder.current_segment()
    if not segment:
        logger.debug('Skipping subsegment creation because of missing segment')
        return False
    current_level = getattr(segment, '_level', default_loglevel)
    if level > current_level:
        logger.debug('Skipping subsegment creation because of loglevel')
        return False
    return True


def set_loglevel(level):
    """
    Set loglevel for currently active segment
    """
    segment = xray_recorder.current_segment()
    if not segment:
        raise ValueError('No current segment, cannot set its loglevel')
    segment._level = level


def capture(name=None, handle_iter=True, level=0):
    """
    This is similar to `xray_recorder.capture()
    <aws_xray_sdk.core.AWSXRayRecorder.capture>`
    but has an additional capability of properly handling iterator functions:
    if function returns an iterable (and if not prohibited explicitly)
    then it will yield a different sub-subsegment for each "iterator chunk".
    I.e. single subsegment for initialization,
    single subsegment for each item generation
    and single subsegment for finalization.

    Also we try to properly handle class method's names
    and include class name: report them as ``MyClass.meth``, not just ``meth``.

    .. note:: we don't properly handle nested classes in python2
       because it doesn't provide ``__qualname__`` value.

    :param str name:
        override function name
    :param bool handle_iter:
        whether to instrument each step of iterator returned by the function
    :param int level:
        "loglevel" for this function's subsegments.
        If this level is *higher* than activae loglevel
        than this function won't generate any subsegment.
    """
    @wrapt.decorator
    def wrapper(wrapped, instance, args, kwargs):
        if not capture_is_active:
            # it will be reported only once by default;
            # this is to avoid confusion caused by non-working .capture
            warnings.warn('XRay capture is inactive')
            return wrapped(*args, **kwargs)

        if not check_loglevel(level):
            # it will produce debug log in this case
            return wrapped(*args, **kwargs)

        # Check if we want to sample out this subsegment
        if not xray_recorder.context.check_subsegment_unsampling():
            # it already logged the reason
            return wrapped(*args, **kwargs)

        func_name = name
        if not name:
            if hasattr(wrapped, '__qualname__'):
                # py3; qualname contains full "path" including nested classes
                func_name = wrapped.__qualname__
            else:
                # py2; let's try to determine correct class name
                func_name = wrapped.__name__
                classname = None
                if instance is not None:
                    func_name = '{}.{}'.format(
                        getattr(instance, '__name__', None) or
                        instance.__class__.__name__,
                        func_name,
                    )

        if handle_iter:
            wrapped, args, kwargs = (
                _wrap_iter,
                (wrapped, args, kwargs, func_name),
                {},
            )

        return xray_recorder.record_subsegment(
            wrapped, instance, args, kwargs,
            name=func_name,
            namespace='local',
            meta_processor=None,
        )

    if callable(name):
        # we were called with no arguments; tolerate it
        wrapped, name = name, None
        return wrapper(wrapped)
    return wrapper


@contextlib2.contextmanager
def within_segment(name, **kwargs):
    """
    within_segment(name, **kwargs)

    Context manager to run nested code within an XRay segment.
    Mostly for testing purposes.
    Can also be used as a decorator, thanks to `contextlib2`__.

    __ http://contextlib2.readthedocs.io/en/latest/

    :param str name: segment name; required.
    :param kwargs: additional arguments for
        :meth:`~aws_xray_sdk.core.AWSXRayRecorder.begin_segment`
    """
    if not capture_is_active:
        warnings.warn('XRay capture is inactive')
        yield None
        return

    segment = xray_recorder.begin_segment(name=name, **kwargs)
    exception = None
    stack = None

    try:
        yield segment
    except Exception as e:
        exception = e
        stack = traceback.extract_stack()
        raise
    finally:
        if exception:
            segment.add_exception(exception, stack)
        xray_recorder.end_segment()


def get_trace_entity():
    """
    Shortcut for ``aws_xray_sdk.core.xray_recorder.get_trace_entity``.
    Will return current trace segment or subsegment,
    or `None` if there is no active segment.
    """
    return xray_recorder.get_trace_entity()


def flush():
    """
    Flush current segment, making sure it is sent to xray.
    Reason: in current xray sdk implementation (aws-xray-sdk==0.96),
    in-progress entities are never sent to the daemon, I don't know why.
    As a result, it first sends all subsegments (without parent segment)
    and only then, after it is finished, it sends the root segment doc.
    This leads to impossibility to observe in-progress data in the console.

    For more details, see ``Segment.ready_to_send`` and
    ``Recorder.end_subsegment`` code in xray sdk.

    When you call this `flush` function, it will try to send
    all currently active entities (segment and all its subsegments).
    It might be excessive because subsequent calls to :func:`flush`
    will re-send at least some of already-sent documents,
    but it should help.
    Call it whenever you feel your function have already run
    for some long enough time.
    """
    # First try implementation: just send current segment,
    # without checking its children.
    # XXX will it work properly? size limits?

    # flush excessive subsegments to make sure our segent is not too large
    xray_recorder.stream_subsegments()
    # then send current segment with its (not-yet-streamed) children
    segment = xray_recorder.current_segment()
    xray_recorder.emitter.send_entity(segment)
