"""
This module allows to decrypt KMS-encoded strings.
Preferred way to do it is with :func:`kms_decrypt` shorthand function.

KMS decryption can be bypassed by setting
:envvar:`DEBUG_KMS_BYPASS` env variable to nonempty string;
in such case this module won't call actual KMS
but instead will return the original value unchanged.

.. envvar:: DEBUG_KMS_BYPASS

   Ignore KMS completely. :meth:`KMS.decrypt` will return original value
   unchanged (note that :meth:`KMS.decrypt_binary` is not affected).
   This is useful for testing,
   when no real KMS credentials is available.
"""

import base64
import logging
import os

import boto3


logger = logging.getLogger(__name__)


class KMS(object):
    """
    Simple wrapper for AWS KMS service.
    """
    def __init__(self):
        self.kms = boto3.client(
            'kms',
            region_name=os.environ.get('kms-region'))

    def decrypt_binary(self, blob):
        """
        Decrypt binary data and return resulting plain-text.
        """
        ret = self.kms.decrypt(CiphertextBlob=blob)
        if not ret.get('KeyId'):
            # probably not required as decrypt() will raise itself
            raise ValueError('Decryption failed?')
        return ret['Plaintext']

    def decrypt(self, b64blob):
        """
        Take blob encoded with base64, decode and then decrypt it.
        """
        if os.environ.get('DEBUG_KMS_BYPASS'):
            return b64blob
        blob = base64.decodestring(b64blob)
        return self.decrypt_binary(blob)


def get_kms():
    """ Convenience wrapper for `KMS` constructor """
    return KMS()


# shorthand
def kms_decrypt(b64):
    """
    Shorthand for ``get_kms().decrypt(b64)``.
    This is the preferred way to use this module.
    """
    return get_kms().decrypt(b64)
