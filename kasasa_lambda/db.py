import logging
import os
import warnings

from .s3 import get_secrets
from .compat import urlparse


logger = logging.getLogger(__name__)


def parse_db_envvars(secrets=None, database=True, include_scheme=False,
                     **defaults):
    """
    Obtain database credentials from environment variables.
    Returns dict of :PEP:`DBAPI <249>`\ -standard credentials
    to be passed as kwargs to `pymysql.connect()`, `psycopg2.connect()` etc.

    Credentials can be one of the following:

    1. `DATABASE_URL` variable containing all the info
       (maybe except for password) -
       will be parsed using :func:`parse_db_url`
    2. Separate ``DATABASE_HOST``, ``DATABASE_PORT``,
       ``DATABASE_USERNAME`` vars
    3. Separate ``HOST``, ``PORT`` and ``USERNAME`` vars

    Password can be specified in one of the following ways:

    1. Inline in ``DATABASE_URL``
    2. Plain text in ``DATABASE_PASSWORD`` or ``PASSWORD`` variable
    3. In secrets key referenced by ``DATABASE_PASSWORD_KEY`` env var
       (e.g. ``mysql_password``);
       in such case secrets should be passed directly to the function
       or will be loaded using sane defaults for env vars.

    :param dict secrets:
        dictionary with secrets, as obtained by
        :func:`~kasasa_lambda.s3.get_secrets`.
        If not provided then we'll try to fetch it ourselves.
    :param str,bool database:
        database name to use. When `True`, we will try to fetch it
        from environment variable ``DATABASE``.
        If string then will use it directly.
        If `False` or `None` then will not specify database at all.
    :param bool include_scheme:
        whether to return URL scheme as a ``scheme`` field in the dict.
        Defaults to `False`.
    """
    # we use our own environment dictionary
    # which is enhanced with the defaults we were provided with
    environ = defaults.copy()
    environ.update(os.environ)

    def anyenv(*keys):
        for key in keys:
            if key in environ:
                return environ[key]
        raise KeyError('None of %s was found in environment' % (keys,))

    def getenv(var):
        # first try DATABASE_var then plain var
        return anyenv('DATABASE_' + var, var)

    def hasenv(var):
        try:
            return getenv(var)
        except KeyError:
            return False

    # first load params from url, if any
    params = {}
    if 'DATABASE_URL' in environ:
        logger.info('Using database URL')
        url_scheme, params = parse_db_url(environ['DATABASE_URL'])
        if include_scheme:
            params['scheme'] = url_scheme
        # strip None values
        params = {
            k: v for k, v in params.items()
            if v is not None
        }
    else:
        logger.info('DATABASE_URL not provided, '
                    'expecting separate credentials for database')
        params['host'] = getenv('HOST')
        if hasenv('PORT'):
            params['port'] = int(getenv('PORT'))
        params['user'] = getenv('USERNAME')

    # don't support plain PASSWORD_KEY to avoid clash with old approach
    # where PASSWORD_KEY denoted secrets filename in the bucket
    # (see map-data-pipeline-*)
    if 'DATABASE_PASSWORD_KEY' in environ:
        logger.info('Using database password from secrets')
        if not secrets:
            secrets = get_secrets(
                # TODO put these vars to secrets as default ones?
                # Bucket: DATABASE_SECRETS_BUCKET or DATABASE_BUCKET or BUCKET
                getenv('SECRETS_BUCKET') or environ['BUCKET'],
                # Key: DATABASE_SECRETS_KEY or SECRETS_KEY or fixed value
                getenv('SECRETS_KEY') or 'secrets/passwords.json',
            )
        if params.get('password'):
            logger.warning('Overwriting URL password with secrets file')
        params['password'] = secrets.get(environ['DATABASE_PASSWORD_KEY'])
    elif hasenv('PASSWORD'):
        logger.info('Using plain-text database password')
        if params.get('password'):
            logger.warning('Overwriting URL password with env-var')
        params['password'] = getenv('PASSWORD')
    elif 'password' not in params:
        raise KeyError(
            'Could not find database password in URL, '
            'plain-text (DATABASE_PASSWORD) '
            'or secrets (DATABASE_PASSWORD_KEY)')

    if database is True:
        # read it from environment
        if 'DATABASE' in environ:
            params['database'] = anyenv('DATABASE')
            logger.info('Using database %s' % params['database'])
        elif 'database' in params:
            logger.info('Database name was specified in DATABASE_URL')
        else:
            raise KeyError(
                'DATABASE not specified and DATABASE_URL is not used')
    elif database:
        # specific value is passed
        params['database'] = database
    elif 'database' in params:
        # db was not requested but obtained (probably from url) -
        # warn the user if it is not empty and remove
        if params['database']:
            logger.warning('Ignoring database value %s', params['database'])
        del params['database']

    return params


def parse_db_url(database_url):
    """
    Returns `tuple` of ``(scheme, params)``
    where scheme is URL ``scheme`` which can be used to infer required db type
    and ``params`` is a `dict`,
    like the one returned by :func:`parse_db_envvars`.

    :param str database_url: original URL containing all the data.
        Example with all supported stuff::

            mysql://user:pass@host:1234/dbname?param1=val&param2=val
    """
    parsed = urlparse.urlparse(database_url)
    params = dict(
        host=parsed.hostname,
        user=parsed.username,
        password=parsed.password,  # it will default to '' anyway
        database=parsed.path.lstrip('/') or None,
        port=parsed.port,  # it is already int
    )
    # parse parameters, if any
    params.update(dict(urlparse.parse_qsl(parsed.query)))
    return parsed.scheme, params


def get_mysql_connection(database_url=None, dict_cursor=False,
                         unbuffered=False, **kwargs):
    """
    Obtain `pymysql.Connection <pymysql.connections.Connection>` instance
    for given DB url.

    .. todo:: Support other database engines here, at least psycopg2 ?

    :param str database_url: URL containing database connection parameters.
        Optionally its parts might be overriden using kwargs.
        URL can be omitted, then all of the required components
        are expected as kwargs.
        URL component names:
        ``host``, ``port``, ``user``, ``password``, ``database``.
        Required components: ``host``, ``database``.

    :param bool dict_cursor: use `~pymysql.cursors.DictCursor` for cursors,
        so that cursor will return records as dicts rather than tuples.
        Shorthand for ``cursorclass=``\ `pymysql.cursors.DictCursor`.

    :param bool unbuffered:
        use `~pymysql.cursors.SSCursor`
        which does not buffer all response data in memory.
        The downside is that it cannot report number of records
        and navigate them only forwards.
        This option can be combined with ``dict_cursor``
        which will result in `~pymysql.cursors.SSDictCursor`.

    :param kwargs: additional attributes to pass to `pymysql`.
        Common examples are ``password`` or ``database``.
    """
    logger.debug('Obtaining DB connection...')

    # import it locally so that other functions will work for psycopg2
    import pymysql

    params = dict()
    if database_url is not None:
        _, params = parse_db_url(database_url)
    params['cursorclass'] = getattr(pymysql.cursors, '{}{}Cursor'.format(
        'SS' if unbuffered else '',
        'Dict' if dict_cursor else '',
    ))
    params.update(kwargs)

    if not all(params.get(k) for k in ('host', 'user', 'password')):
        raise ValueError(
            'Host, user and password are mandatory '
            'and should be provided either in URL or as kwargs'
        )

    printparams = params.copy()
    printparams['password'] = '*' * len(params['password'] or '')
    logger.info(
        'Connecting to DB using these credentials (password masked): %s',
        printparams,
    )

    conn = pymysql.connect(**params)

    logger.debug('Obtained DB connection: %s', conn)

    return conn


def get_db_connection(*args, **kwargs):
    warnings.warn(DeprecationWarning(
        'Please use get_mysql_connection instead'))
    return get_mysql_connection(*args, **kwargs)
