"""
Helper for AWS STS (Security Token Service).
Provides access to ``AssumeRole`` method
which is required for cross-account resource access.

Please see `External ID docs`_ for details on the workflow
which current implementation is intended for.

In short:

1. *We* generate a unique External ID;
2. *We* tell *them* our AWS account number and that External ID;
3. *They* create an IAM role with ``Principal`` set to our AWS account number
   and ``Condition`` to check that ``ExternalID`` is the one we gave them;
4. *They* tell *us* an ARN of that newly created IAM role;
5. When we need to access resources on *their* account,
   *we* create special session and use it as follows:

    session = get_sts_session(
        role=ROLE_ARN,
        external_id=EXTERNAL_ID,
    )
    s3 = get_s3(BUCKET_NAME, session=session)
    ...

"""

import boto3
import os


class STS(object):
    """
    We use default credentials
    (most likely from environment variables or from Lambda runner)
    to obtain custom credentials.
    """
    def __init__(self):
        self.sts = boto3.client('sts')

    def assume_role(self, role, session_name=None, external_id=None,
                    ttl_minutes=None):
        """
        :param str role: ARN of the desired role
        :param str external_id:
            semi-secret value used to access other account's IAM role.
            See `External ID docs`_ for details.
        :param str session_name:
            a string identifying this usage of the requested role.
            You can pass a custom value, or
            by default we'll try to use current lambda function name.
            Note that this value is required by STS,
            so it is mandatory to pass when working in non-Lambda context.
        :param int ttl_minutes: optional credentials expiry timeout in minutes.
            Defaults to one hour.
            Minimum is 15 minutes.

        .. _`External ID docs`: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user_externalid.html
        """
        if not session_name:
            session_name = os.environ.get('AWS_LAMBDA_FUNCTION_NAME')
            if not session_name:
                raise ValueError(
                    'session_name not provided and we are not in lambda')

        if ttl_minutes is None:
            ttl_minutes = 60  # that is the default value
        elif ttl_minutes < 15:
            raise ValueError('Minimum TTL is 15 minutes, not %d' % ttl_minutes)

        return self.sts.assume_role(
            RoleArn=role,
            RoleSessionName=session_name,
            DurationSeconds=ttl_minutes * 60,
            ExternalId=external_id,
        )['Credentials']

    def get_session(self, *args, **kwargs):
        creds = self.assume_role(*args, **kwargs)
        return boto3.Session(
            aws_access_key_id=creds['AccessKeyId'],
            aws_secret_access_key=creds['SecretAccessKey'],
            aws_session_token=creds['SessionToken'],
        )


def get_sts():
    return STS()


def get_sts_session(*args, **kwargs):
    return get_sts().get_session(*args, **kwargs)
