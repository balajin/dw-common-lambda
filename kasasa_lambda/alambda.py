"""
For now this module allows launching Lambda functions,
passing them payload and obtaining their result
(when using :meth:`~Lambda.invoke_and_wait` for launching).

If you want to recursively invoke lambda function from itself
then you will want to get its name
from :envvar:`AWS_LAMBDA_FUNCTION_NAME` environment variable.
Alternatively you can just pass `None` for function name,
in which case current function will be assumed.

.. envvar:: AWS_LAMBDA_FUNCTION_NAME

   This variable is available in Lambda environment
   and contains current function's name. See `Lambda docs`_ for details.

.. envvar:: AWS_LAMBDA_FUNCTION_VERSION

   Holds the version qualifier of currently runing Lambda.
   See `Lambda docs`_ for details.

.. _`Lambda docs`: https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html#lambda-environment-variables
"""

import base64
import json
import os

import boto3


class Lambda(object):
    """
    Represents a Lambda function.
    Can be constructed from either function name or ARN.

    Attributes:

        lam (`Lambda.Client`): Underlying instance of Boto3's Lambda client.
        name (str): Name or ARN of the lambda,
            whichever was passed to the constructor
        version (str,None): version qualifier of the lambda function,
            or `None` for ``$LATEST``.
    """
    def __init__(self, name_or_arn, version=None):
        """
        :param str name_or_arn: either function name or ARN of the function.
            Both are allowed by underlying AWS API.
            If set to `None` then defaults to current function's name
            as defined by :envvar:`AWS_LAMBDA_FUNCTION_NAME`.
            Will then also respect version.
            Alternatively you can pass an ARN
            which can be obtained from the ``context`` variable:
            ``context.invoked_function_arn``.

        :param str version: version qualifier to use.
            If ``name_or_arn`` is `None`
            then version will be the same as for currently running function
            (:envvar:`AWS_LAMBDA_FUNCTION_VERSION`).
        """
        if name_or_arn is None:
            # name_or_arn was not passed ->
            # we want to use our own (current) function name.
            # Note that this should be requested explicitly.
            # Current function name or ARN could also be obtained
            # from ``context`` passed to the Lambda function,
            # but we have no access to that context object
            # hence use environment value.
            name_or_arn = os.environ['AWS_LAMBDA_FUNCTION_NAME']
            version = os.environ['AWS_LAMBDA_FUNCTION_VERSION']

        self.lam = boto3.client('lambda')
        # make sure name is valid;
        # if name/arn is inavlid then this call will raise
        # ResourceNotFoundException
        self.lam.get_function_configuration(FunctionName=name_or_arn)
        self.name = name_or_arn
        self.version = version

    def _prepare_payload(self, payload):
        if payload is None:
            return payload

        if hasattr(payload, 'read'):
            # suppose we want to read it from a file-like object
            return payload

        # everything else will be JSON-encoded.
        # That is because payload is required to be a JSON.
        payload = json.dumps(payload)

        # make sure it is binary (important for py3)
        return payload.encode('utf-8')

    def _invoke(self, payload, context, **kwargs):
        if context:
            # FIXME is there a better way?
            if not isinstance(context, dict) or 'custom' not in context:
                context = {'custom': context}
            kwargs['ClientContext'] = base64.b64encode(json.dumps(context))

        if self.version:
            # we cannot pass Qualifier=None, hence use kwargs
            kwargs['Qualifier'] = self.version

        if payload is not None:
            kwargs['Payload'] = self._prepare_payload(payload)

        return self.lam.invoke(
            FunctionName=self.name,
            **kwargs
        )

    def invoke_async(self, payload=None, context=None):
        """
        Fire off this lambda function
        but don't wait for its completion or result.

        :param payload: optional payload
            (binary, string or JSON-encodable `dict` or `list`)
        :param context: client context,
            see :meth:`.invoke_and_wait` for details.

            .. warning:: For now (19 Feb 2018) context does not work
               when launching function asynchronously;
               lambda just silently ignores client context
               when function is launched in async way.
               So this parameter is supported only in hope that
               it will work in the future.
        """
        self._invoke(
            payload, context,
            InvocationType='Event',
        )

    def invoke_and_wait(self, payload=None, context=None, get_logs=False):
        """
        Launch specified function and wait for it to finish.
        If ``get_logs`` is `True` then will return tuple ``(result, logs)`` -
        notice that only last ``4kb`` of logs are retrieved;
        else will return just result.
        On failure will raise an `Exception`.

        .. todo:: Are we properly handling exceptions?

        :param payload: optional payload
            (binary, string or JSON-encodable `dict` or `list`)
        :param bool get_logs: whether to retrieve lambda's logs.
        :param context: custom context to pass to the lambda function.
            It can be either of:

            * `dict` with a ``custom`` key: will be passed as is
              (for dict format see `PutEvents docs`__);
            * anything else (JSON-encodable): will be passed
              as a ``custom`` value of the context.

        __ https://docs.aws.amazon.com/mobileanalytics/latest/ug/PutEvents.html

        :returns: either Lambda's result or a `tuple` of ``(result, logs)``,
            depending on ``get_logs`` argument value.

        :raises: an `Exception` if the underlying lambda fails.
        """

        # this will return only after function is done or failed
        result = self._invoke(
            payload, context,
            InvocationType='RequestResponse',
            LogType='Tail' if get_logs else 'None',
        )
        r_payload = result['Payload'].read()
        r_payload = json.loads(r_payload)

        if get_logs:
            logs = base64.decodestring(result['LogResult'])
            return r_payload, logs
        return r_payload


def get_lambda(*args, **kwargs):
    """
    Convenience wrapper for `Lambda` constructor.
    """
    return Lambda(*args, **kwargs)
