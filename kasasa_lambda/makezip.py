#!/usr/bin/env python
# This script creates a zipped bundle with given script and all its deps
# and uploads it to certain lambda function(s).
# Optionally it can include some additional data into the bundle.
#
# Unlike existing packages lambda_deploy, lambda-deploy and python-lambda
# this one supports S3 uploading
# and allows bundling extra data.

"""
This submodule is intended to be used as a stand-alone console command.
It will generate a Lambda bundle from the code in current directory
and will upload it to the given Lambda function.

One important feature which is often missing in competing solutions
is an ability to include certain external files into the bundle,
i.e. files which are not Python code.
It is done with ``--extra`` option.

Argparse docs (also available with ``--help`` option)
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. argparse::
   :ref: kasasa_lambda.makezip.make_parser
   :prog: makezip
"""

import argparse
import datetime as dt
import os.path as op
import os
import re
import shutil
import tempfile
import zipfile

import boto3
try:
    import pip._internal.main as pip_main
except ImportError:
    # pip 9.0.1 (and maybe some newer) used this path
    import pip.main as pip_main

from . import get_s3


# These deps are pre-installed in Lambda.
# This is not an exhaustive list, just the most heavy-weight packages.
# FIXME maybe check versions? As we want to include newer version of package
# if it is required and we know lambda ships only old one
EXCLUDE_DEPS = [
    'botocore',
    'awscli',
    'docutils',
    'yum',
]


def recwrite(zf, d, dest=None, ignore=None):
    """
    Add given file or directory to the zipfile,
    recursing into directories when these are encountered.
    :param ignore: if set to a regexp or pattern,
    will skip matching items when scanning directories.
    """
    if not dest:
        dest = op.basename(d)

    # first of all, anyway write d to dest - be it file or directory;
    # for directory it will create corresponding entry.
    zf.write(d, dest)

    # if that was not a directory then we are done
    if not op.isdir(d):
        return

    # now, for directories we want to scan them
    for item in os.listdir(d):
        if ignore and re.search(ignore, item):
            continue
        recwrite(zf, op.join(d, item), op.join(dest, item), ignore=ignore)


def install_deps(reqdir):
    print('--- Installing dependencies from requirements.txt to tmpdir ---')
    # install these requirements to our temp dir
    pipres = pip_main(['install', '-r', 'requirements.txt', '-t', reqdir])
    # pip might raise SystemError if some error occurs,
    # but that is normal for us as well -
    # because we don't expect any error in pip
    if pipres != 0:
        raise Exception(
            'pip installing requirements failed with code {}'
            .format(pipres),
        )
    print('--- Dependencies installed ---')

def prepare_bundle(zf, extras):
    """
    Install all requirements and pack them to given zipfile
    """
    reqdir = tempfile.mkdtemp(suffix='-reqs')
    try:
        # install these requirements to our temp dir
        install_deps(reqdir)

        # now that requirements are installed, let's pack them
        print('--- Packing files and dependencies to the zip file ---')
        # first pack our lambda function code -
        # by default all python files and packages found in the dir,
        # except for `tests` package.
        # TODO make this configurable, both filelist and exceptions.
        print('-- Packing python packages from current dir --')
        for name in os.listdir('.'):
            if name.startswith('.'):
                # we don't want to include hidden dirs here, like .git
                # (unlike deps tmpdir where dotfiles are required)
                continue
            if name == 'tests':
                # don't include tests
                continue
            path = name  # as that is in CWD
            if not any([
                path.endswith('.py'),
                op.exists(op.join(path, '__init__.py')),
            ]):
                # not a python package (either a py-file or pkg dir)
                continue
            # add this, but exclude any .pyc files in the dir
            recwrite(zf, path, name, ignore=r'\.pyc$')

        # now add all installed deps, excluding what the user asked
        print('-- Packing installed dependencies from temp dir --')
        for name in os.listdir(reqdir):
            # this also includes hidden items - exactly as we want
            path = op.join(reqdir, name)

            # determine related package name, for filtering purposes
            bname = name
            if name.endswith('.py'):
                bname = name[:3]
            elif name.endswith(('.pyc', '.pyo')):
                bname = name[:4]
            elif name.endswith(('.egg-info', '.dist-info')):
                # also remove version
                # full dirname may look like 'package_name-1.2.5.dist-info'
                bname = name.split('-', 1)[0]

            if bname in EXCLUDE_DEPS:
                # this package is known to be available in lambda,
                # so don't include it
                # FIXME maybe check versions? see above
                continue

            recwrite(zf, path, name, ignore=r'\.pyc$')

        # now also add extra data requested by user
        if extras:
            print('-- Packing requested extras --')
            for path in extras:
                # these are expected to be full or rel paths
                name = op.basename(path)
                recwrite(zf, path, name)  # nothing to ignore here

    finally:
        print('--- Removing temporary directory with dependencies ---')
        shutil.rmtree(reqdir)


def make_parser():
    parser = argparse.ArgumentParser(
        description='Create a zip bundle from current package with all deps '
        'and upload it to AWS lambda function',
    )
    parser.add_argument('lambdafn', help='AWS Lambda function identifier')
    parser.add_argument(
        '-b', '--bucket', type=str,
        help='Bucket name to use. Mandatory if zip archive is >10mb')
    parser.add_argument(
        '-z', '--zipfile', type=str,
        help='Name of zipfile to use. '
        'By default will generate random filename based on lambda name.')
    parser.add_argument(
        '-e', '--extra', action='append', dest='extras',
        help='Extra files or directories to include to the zip')
    parser.add_argument(
        '-o', '--overwrite', action='store_true',
        help='Allow overwriting existing zipfile in the bucket')
    parser.add_argument(
        '-d', '--delete', action='store_true',
        help='Delete zipfile from the bucket after uploading')
    return parser


def main(args=None):
    args = make_parser().parse_args(args)

    # create temp dir and install dependencies into it
    if not op.exists('requirements.txt'):
        parser.error('Requriements file not found')

    with tempfile.NamedTemporaryFile(
        prefix='tmp-{}'.format(args.lambdafn),
        suffix='.zip',
    ) as ztmp, zipfile.ZipFile(
        ztmp, 'w',
        compression=zipfile.ZIP_DEFLATED,
    ) as zf:
        # install requirements and pack everything related to the bundle
        prepare_bundle(zf, args.extras)

        # zipfile is ready, let's close and upload it
        zf.close()  # this will close&flush zip helper but leave actual file
        ztmp.seek(0)  # rewind pointer

        # now we want to upload the bundle -
        # either directly to lambda or via s3
        zipname = args.zipfile or '{}-{}.zip'.format(
            args.lambdafn,
            dt.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
        )
        if args.bucket:
            # use bucket
            print('--- Uploading zipfile to S3 bucket ---')
            s3 = get_s3(args.bucket, force_real=True)
            s3.upload(ztmp.name, zipname, overwrite=args.overwrite)
            params = {
                'S3Bucket': args.bucket,
                'S3Key': zipname,
            }
        else:
            print('--- Will upload zipfile directly to lambda ---')
            if op.getsize(ztmp.name) > 10*1024*1024:
                print('WARNING: file is too large, direct upload will fail')
            params = {
                'ZipFile': ztmp.read(),
            }

    # tempfile can be closed&removed now
    # now call lambda
    print('--- Deploying our bundle to lambda function {} ---'.format(
        args.lambdafn))
    lam = boto3.client('lambda')
    lam.update_function_code(FunctionName=args.lambdafn, **params)

    if args.bucket and args.delete:
        print('--- Removing zipfile from the bucket as requested ---')
        s3.remove(zipname)

    print('--- Done! ---')


if __name__ == '__main__':
    main()
