"""
Obtain helper object `SQS` instance for desired queue
using :func:`get_sqs` function.
"""

import json
import logging

import boto3

from kasasa_lambda import xray_optional as xray


logger = logging.getLogger(__name__)


class Message(object):
    """
    Represents a received message
    and can be used as a context manager
    to automatically confirm its handling on successful exit

    Attributes:

        sqs: The `SQS` instance this message belongs to.
        msgid (str): unique identifier of the message.
            Corresponds to `SQS.Message.message_id`.
        handle (str): special identifier used to delete this message.
            Corresponds to `SQS.Message.receipt_handle`.
        body (str): message body in textual form.
            Corresponds to `SQS.Message.body`.
    """
    def __init__(self, sqs, msgid, handle, body):
        self.sqs = sqs
        self.msgid = msgid
        self.handle = handle
        self.body = body

    @property
    def data(self):
        """
        Cached access to JSON-decoded message body.
        If decoding fails then this property returns
        the same as `body` attribute.
        """
        if not hasattr(self, '_data'):
            try:
                self._data = json.loads(self.body)
            except ValueError:
                logger.warning('Could not decode message body: %s',
                               self.body)
                self._data = self.body
        return self._data

    def __enter__(self):
        logger.info('Start processing %s', self)
        return self

    def __exit__(self, *exc):
        if exc == (None, None, None):
            logger.info('Done processing %s; will delete', self)
            self.delete()
        else:
            logger.error('Failed processing %s; won\'t delete it', self,
                         exc_info=exc)

    def __str__(self):
        return '<Message {}: {}>'.format(self.msgid, self.body)

    @xray.capture
    def delete(self):
        """
        Explicitly delete this message,
        i.e. mark it as processed.

        The preferred way to delete the message
        is to wrap the code processing it in a ``with`` statement like this::

            with my_message:
                process(my_message)

        That way the message will only be deleted
        if processing went successfully, i.e. didn't raise any exception.
        (Note that StopIteration for example will be considered an exception
        and thus will be recognized as processing failure.)
        """
        self.sqs.delete(self.handle)

    @xray.capture
    def return_to_queue(self):
        """
        Immediately return this message back to the queue.
        This is somewhat reverse to :meth:`.delete`.
        Delete marks the message as processed,
        while this method marks the message as not-processed
        without waiting for the timeout.

        .. note::
           This method should only be used when
           you didn't even start processing the message.
           If you tried to process the message but failed
           then don't return it to queue:
           instead let it be returned automatically after a timeout.
           That way you probably won't bump into this message again and again
           when using :meth:`SQS.receive_all` or similar approaches.
        """
        m = self.sqs.queue.Message(self.handle)
        # set timeout to 0 for this particular message
        # (and this particular attempt only),
        # thus immediately returning it.
        m.change_visibility(VisibilityTimeout=0)


class SQS(object):
    """
    This class represents single SQS queue.

    For now it only supports receiving messages, not sending.

    Attributes:

        sqs (`SQS.ServiceResource`): Underlying Boto3's SQS resource
        queue (`SQS.Queue`): Underlying Boto3's Queue object

    .. todo:: document __len__ here?
    """
    @xray.capture
    def __init__(self, name_or_url):
        """
        :param name_or_url: either name (without colon)
            or URL (starting with ``http:`` or ``https:``) of the queue.
            ARN is not supported.
        """
        self.sqs = boto3.resource('sqs')

        if ':' in name_or_url:
            # this is most likely URL - use it
            self.queue = self.sqs.Queue(name_or_url)
            # and make sure url is valid
            # May raise QueueDoesNotExist for missing queue
            # or ClientError for invalid address
            self.queue.load()
        else:
            # it is name - let's resolve
            # May raise QueueDoesNotExist
            self.queue = self.sqs.get_queue_by_name(QueueName=name_or_url)

    @xray.capture
    def receive(self, max_count=1, wait=1):
        """
        Receive up to ``max_count`` messages from the queue.

        :param max_count: how many messages to return.
            Maximum value is ``10``, as stated in
            :meth:`boto3.SQS.Queue.receive_messages` docs.

            .. note::
               It is not guaranteed that this method will return
               exactly ``max_count`` messages,
               even if there are more messages available in the queue.
               It often returns just one message.

               See :meth:`receive_max` if you need to overcome this.

        :param wait: how many seconds to wait for new messages,
            set to `None` to return immediately
            (in which case some messages may not be noticed).
            Setting ``wait`` to ``0`` or `None` will disable long-polling,
            See :meth:`boto3.SQS.Queue.receive_messages` docs
            and `SNS docs`__
            to understand the reason.

        __ https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-long-polling.html

        :returns: `list` of up to ``max_count`` `Message` objects
        """
        args = {
            'MaxNumberOfMessages': max_count,
        }
        if wait is not None:
            args['WaitTimeSeconds'] = wait
        msgs = self.queue.receive_messages(**args)

        logger.info('Got %(count)d message(s)', dict(count=len(msgs)))

        return [
            Message(self, m.message_id, m.receipt_handle, m.body)
            for m in msgs
        ]

    @xray.capture
    def receive_all(self, wait=1, return_unused=True):
        """
        Receive and yield :class:`Message`\ s until queue is exhausted.
        This is primarily useful for non-Lambda environment
        when there is no time limit
        but we want to handle all available messages.

        :param int wait:
            ``wait`` is required to be not-`None`,
            or else some messages may be skipped.
            See :meth:`receive` for details.
            Note that this timeout will be used for every batch.
        :param bool return_unused:
            if generator deleted but not exhausted
            (like, the function processed 5 messages while last batch had 10
            and then terminated),
            then remaining messages would hang `"in flight"`__
            until their visibility timeout expires.
            When this flag is set to `True` (default),
            we will try to release such messages immediately.

            __ https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html#inflight-messages
        """
        if not wait:
            raise ValueError(
                'receive_many won\'t work properly in short-poll mode')
        while True:
            # request them in batches of 10
            # to reduce the number of requests (which are billed).
            # We cannot request more than 10 messages anyway.
            msgs = self.receive(
                max_count=10,
                wait=wait,
            )
            if not msgs:
                logger.info(
                    'No messages received in this batch; '
                    'probably the queue is exhausted.')
                break
            try:
                while msgs:
                    yield msgs.pop(0)
            finally:
                if msgs:
                    # probably we were terminated
                    # (finally block is called in iterator's __del__)
                    # and there are some "unused"/unprocessed messages;
                    # try to return them immediately.
                    # If we weren't terminated then msgs is just empty.
                    for m in msgs:
                        m.return_to_queue()
                    logger.info('Returned %(returned)d messages to queue',
                        dict(returned=len(msgs)))

    def receive_max(self, max_count=10, wait=1):
        """
        Like :meth:`receive` but will try its best to retrieve
        at least ``max_count`` messages if there are enough messages in queue.
        Also it can accept ``max_count > 10``.
        """
        assert wait > 0, 'wait should be non-zero'
        while True:
            msgs = self.receive(min(max_count, 10), wait=wait)
            if not msgs:
                # no messages returned -> no more messages available
                break
            for m in msgs:
                yield m
            max_count -= len(msgs)
            if max_count <= 0 or len(self) == 0:
                # FIXME do we really need to check len(self)?
                break

    def delete(self, handle):
        """
        Delete message with given handle from the queue,
        i.e. mark it as processed.

        .. deprecated:: 1.8.0
            Don't use this method directly, use :meth:`Message.delete` instead.

        :param str handle: `~SQS.Message.receipt_handle`
            of the message to delete
        """
        m = self.queue.Message(handle)
        m.delete()

    def attributes(self, refresh=False):
        """
        Returns queue attributes as a dict.
        Note that they are cached on boto3 side by default.

        :param bool refresh: set it to True
            to reload values with
            :meth:`sqs.queue.load() <SQS.Queue.load>`
            before returning them.
        """
        if refresh:
            self.queue.load()
        return self.queue.attributes

    def attribute(self, name, refresh=False):
        """
        Return a single attribute from :meth:`.attributes`.

        ``ApproximateNumberOfMessages`` attribute can also be accessed
        using `.__len__`.

        :param str name: attribute name (case sensitive).
        :param bool refresh: set to True to reload attributes
            before fetching this one.
        """
        return self.attributes(refresh).get(name)

    def __len__(self):
        """
        len(sqs) is a shorthand to access
        approximate number of available messages
        WARNING: it is cached, call sqs.queue.load() to refresh
        """
        # attribute is in string form, so we need to parse it
        return int(self.attribute('ApproximateNumberOfMessages', refresh=True))


def get_sqs(*args, **kwargs):
    """
    Convenience wrapper for `SQS` constructor.
    """
    return SQS(*args, **kwargs)
