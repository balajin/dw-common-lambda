"""
For now this module supports only publishing messages to SNS topics.
"""

import json

import boto3


class SNS(object):
    """
    This class represents a ``SNS`` topic
    which can be published to.

    Attributes:

        sns (`SNS.ServiceResource`):
            Reference to underlying ``boto3`` ``SNS`` resource.
        topic (`SNS.Topic`):
            Reference to underlying ``boto3``'s ``Topic`` object.
        topic_arn (str):
            Contains topic's `ARN <SNS.Topic.arn>` identifier,
            regardless of what was passed to the constructor.
    """

    TOPIC_NAMES = {}  # map name to arn

    def __init__(self, topic_arn_or_name):
        """
        Create new SNS instance.

        :param str topic_arn_or_name: either ARN or readable name of the topic.
        """
        self.sns = boto3.resource('sns')

        if topic_arn_or_name.startswith('arn:'):
            self.topic_arn = topic_arn_or_name
        else:
            if not self.TOPIC_NAMES:
                for t in self.sns.topics.all():
                    # last part of the arn is topic name,
                    # which itself may contain colons.
                    # So we split to known number of parts
                    # and use the last part.
                    name = t.arn.split(':', 6)[-1]
                    self.TOPIC_NAMES[name] = t.arn
            if topic_arn_or_name not in self.TOPIC_NAMES:
                raise ValueError(
                    'Unknown topic name {!r}, known are {}'.format(
                        topic_arn_or_name, list(self.TOPIC_NAMES),
                    )
                )
            self.topic_arn = self.TOPIC_NAMES[topic_arn_or_name]

        self.topic = self.sns.Topic(self.topic_arn)
        # make sure topic ARN is valid
        self.topic.load()

    def publish(self, message, subject=None):
        """
        Publish given message to the topic.
        If message is dict or list then it will be JSON-encoded.

        :param message: either `str`, `list` or `dict`.

            `list` and `dict` will be JSON-encoded before publishing.

        :param str subject: optional Subject value.
            It is used when sending to email,
            and is also included in JSON object passed to the lambda function
            (and some other places maybe).
            It cannot be larger than 100 chars,
            cannot include line breaks,
            and there are some other limitations -
            see `boto3:SNS.Topic.publish` docs for details.
        """
        # SNS supports a lot of additional features,
        # like attaching Subject or MessageAttributes or TargetArn.
        # But for now we just send around JSON-encoded data in message body,
        # hence other features are not yet implemented.

        # prepare message
        if isinstance(message, (dict, list)):
            message = json.dumps(message)
        if not isinstance(message, str):
            raise TypeError('Invalid message type %s' % type(message))

        params = {
            'Message': message,
        }
        if subject:  # either empty or None means no subject
            # validation will be done by publish() but has cryptic messages
            # so try to pre-validate ourself
            if len(subject) >= 100:
                raise ValueError('Subject is too long')
            if any(ord(c) < ord(' ') for c in subject):
                raise ValueError('Subject contains control characters')
            # also first char should be "letter, number or punctuation",
            # but let's leave it to the boto3 to check

            params['Subject'] = subject
        self.topic.publish(**params)


def get_sns(*args, **kwargs):
    """
    Convenience wrapper for `SNS` constructor.
    """
    return SNS(*args, **kwargs)
