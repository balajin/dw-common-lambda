"""
Attributes:

    PIG_SCRIPTS_BUCKET (str):
        pre-initialized with :envvar:`script_bucket` env variable,
        defaults to ``tt-map-pig-scripts-dev-us-west-2``.

        .. todo:: use more relevant variable name (at least uppercase)
            and don't hardcode default value?

    PIG_VERSION (str): initialized from :envvar:`pig_version`,
        defaults to ``0.17.0``.
"""

from glob import glob
from itertools import chain
import logging
import os
import os.path as op
import sys
import tempfile
from time import sleep
import warnings

import sh as sh_base

from .compat import makedirs_unless_exist, rmtree_if_exists
from .s3 import get_s3
from .misc import touch, rotate_directories


PIG_VERSION = os.environ.get('pig_version', '0.17.0')
PIG_TARBALL = 'pig-{}.tar.gz'.format(PIG_VERSION)
PIG_URL = ('http://archive.apache.org'
           '/dist/pig/pig-{}/{}'.format(PIG_VERSION, PIG_TARBALL))
# first look it up in current directory
PREFETCHED = op.join(op.curdir, PIG_TARBALL)
if not op.exists(PREFETCHED):
    # then in the root directory
    PREFETCHED = '/' + PIG_TARBALL
if not op.exists(PREFETCHED):
    # fallback to downloading
    PREFETCHED = None
    # this might also be overriden when instantiating PigSession

# s3 bucket from which we should take PIG scripts.
# In production it is passed in as env variable "script_bucket",
# but for testing we want to use data baked into our repo -
# so default value should match the name of directory in the repo
PIG_SCRIPTS_BUCKET = os.environ.get(
    'script_bucket', 'tt-map-pig-scripts-dev-us-west-2')


logger = logging.getLogger(__name__)

# pty is not available on some systems, and we don't actually need it,
# so never use it
sh_nopty = sh_base(_tty_out=False)
# this is because sh inspects the above line to find baked-in variable name,
# and doesn't allow using "sh" - see sh.SelfWrapper.__call__
sh = sh_nopty
# variant of sh without capturing stdout/stderr
shnc = sh_base(_out=sys.stdout, _err=sys.stderr)


class PigSession(object):
    """
    This class represents a single PIG session.

    How to use:

    1. Obtain new instance (using :func:`get_pig`, or instantiate directly);
    2. Run :meth:`.ensure_ready` which will install PIG if necessary
       and initialize this instance;
    3. Invoke :meth:`.run` and do whatever you need with the result;
    4. Tell pig that it is not needed anymore -
       eiteher call :meth:`.cleanup` or end context manager block.
       Installed PIG instance will be left there,
       so that subsequent launches won't have to unpack it again.

    Attributes:
        PIGDIR (str): directory where Pig is installed.
            It is initalized with a temporary directory when class is loaded;
            then on first usage we will create it and install pig to it,
            and on subsequent launches we will just re-use that pig.
            If pig is currently being extracted by another thread
            then we will wait for it to finish extracting.

            .. note:: this might result in a dead-lock
                if the other thread dies while extracting a pig.

            .. todo:: fix possible deadlock with pig extraction

        FULLPIGDIR (str): path to the pig binaries, based on `.PIGDIR`.
            Might also be overriden for given instance
            if ``prefetched`` flag is passed to constructor.
        PIG_READY_FLAG (str): filename for pig readiness flag.
        pigcmd: ``sh`` command instance for pig command;
            this is set after initialization.

        INDIR (str): temporary directory where input files are stored
            for given helper instance.
        OUTDIR (str): temporary directory for output files
            generated by helper instance.
    """
    PIGDIR = op.join(
        tempfile.gettempdir(), 'bv', '')  # '' is for trailing slash
    # pigdir including version-specific dir
    # (or just use 'pig-' + PIG_VERSION ?)
    FULLPIGDIR = op.join(PIGDIR, 'pig-{}'.format(PIG_VERSION))
    PIG_READY_FLAG = op.join(PIGDIR, 'pig-ready.flag')
    pigcmd = None

    # These are initialized per instance
    INDIR = None
    OUTDIR = None

    def __init__(self, keep_files=False, prefetched=None):
        """
        Create an instance of PigSession with independent temporary directory.

        :param keep_files: don't delete temporary files on cleanup
        :param prefetched: path to file or directory containing prefetchec PIG.
            Pig version should match environment variable `pigversion`,
            or else you will get FileNotFoundError
        """
        self.keep_files = keep_files
        # use user-provided location (if any) or auto-discovered value (if any)
        self.prefetched = prefetched or PREFETCHED

        self.directory = tempfile.mkdtemp()
        self.INDIR = op.join(self.directory, 'input', '')
        self.OUTDIR = op.join(self.directory, 'output', '')

        self.ready = False
        self._clean = False

    def _install_pig(self):
        logger.info('Preparing the environment to be able to run PIG...')

        # create pig-global directory
        makedirs_unless_exist(self.PIGDIR)

        # don't create in/out directories:
        # indir will be created by session instance
        # and outdir should be created by pig itself

        # download pig unless already present
        if not self.prefetched:
            logger.info('Downloading PIG tarball from %s ...', PIG_URL)
            shnc.curl('-LO', PIG_URL)
        # unpack pig
        pigfile = self.prefetched or op.basename(PIG_URL)
        logger.info('Unpacking PIG...')
        # here we don't want to spam tons of output, so let's catch it
        shnc.tar('xf', pigfile, C=self.PIGDIR)

        # mark that pig is ready
        logger.debug('Touching ready flag %s', self.PIG_READY_FLAG)
        touch(self.PIG_READY_FLAG)

        logger.info('Unpacked and set all the environment to run PIG')

    @classmethod
    def _wait_for_pig(cls):
        logger.info('Someone is preparing PIG - waiting...')
        while not op.exists(cls.PIG_READY_FLAG):
            sleep(1)
        logger.info('Pig is prepared')

    def _prepare_environment(self):
        """
        Internal method to install/deploy pig
        """
        if self.prefetched and op.isdir(self.prefetched):
            logger.info('PIG is already prefetched and unpacked - using it: %s',
                        self.prefetched)
            # prefetched is a directory -> we were provided with unpacked pig
            # to use; so let's just initialize pigcmd and we're done with that
            self.FULLPIGDIR = op.join(
                self.prepared,
                op.basename(op.dirname(PIG_URL)),
            )

        elif not op.exists(self.PIG_READY_FLAG):
            # really not ready yet. But probably someone else is preparing?
            if op.exists(self.PIGDIR):
                # don't interfere, just wait for them to finish preparing
                self._wait_for_pig()
            else:
                # nobody even started preparing pig - so we should do that
                self._install_pig()

        # set pig command
        # (if we try to instantiate it before unpacking
        # then we will get CommandNotFound error)
        self.pigcmd = sh.Command(
            op.join(self.FULLPIGDIR, 'bin', 'pig'),
        )

        logger.info('Environment is ready.')

    def _make_directories(self):
        # ensure indir exists
        makedirs_unless_exist(self.INDIR)
        # outdir should also exist, because actual files will be created in
        # its subdirectories
        makedirs_unless_exist(self.OUTDIR)

    def ensure_ready(self):
        """
        Make sure pig is downloaded and is ready to work.
        """
        # let's unpack pig if required
        # and initialize pigcmd
        self._prepare_environment()

        # now initialize directories for this instance
        self._make_directories()

        self.ready = True

    def _download_script(self, remote, local):
        """
        This is placed in separate method to simplify overriding during tests
        """
        logger.info('Downloading pig script %s to %s...', remote, local)
        get_s3(PIG_SCRIPTS_BUCKET).download(remote, local)

    def _filelist_to_params(self, script, kind, filelist):
        """
        Parse filelist (either infile or outfile option)
        and return params dict for pig.

        :param str script:
            script name, to be used for default filename calculation

        :param str kind:
            param type (input or output), for param name calculation

        :param filelist:
            original value of infile/outfile parameter,
            either string, list, dict, or None
        """
        if filelist is None:
            # default
            filelist = '{}.txt'.format(
                op.splitext(op.basename(script))[0]
            )

        fdir = {'input': self.INDIR, 'output': self.OUTDIR}[kind]

        def mkval(val):
            if kind == 'input':
                if op.isabs(val):
                    raise ValueError(
                        'Input file should be relative to pig.INDIR', val)
                if not op.exists(op.join(fdir, val)):
                    raise ValueError('Input file does not exist',
                                     op.join(fdir, val))
            elif kind == 'output' and op.basename(val) != val:
                raise ValueError('Output files should not include path', val)
            return op.join(fdir, val)

        if isinstance(filelist, list):
            return {
                '{}{}'.format(kind, idx): mkval(val)
                for idx, val in enumerate(filelist, start=1)
            }
        if isinstance(filelist, dict):
            return {
                '{}_{}'.format(kind, key): mkval(val)
                for key, val in filelist.items()
            }
        return {kind: mkval(filelist)}

    def _run_pig(self, params, script):
        """
        :param params: dict of parameters to pass to the pig
        :param script: full or relative path to script file
        (relative to CWD)
        """
        param_args = sum(
            (['-p', '='.join(item)]
             for item in params.items()),
            []
        )

        env = os.environ.copy()
        env['JAVA_HOME'] = '/usr'

        self.pigcmd(
            param_args,  # pass all our params
            x='local',  # execution mode: local
            f=script,  # file to use
            l=self.directory,  # make sure logs are in the right directory
            _env=env, _cwd=self.directory,
            _out=sys.stdout, _err=sys.stderr,
            # XXX do we want to capture output?
        )

    def _glue_parts(self, parts, outfile):
        """
        Take list of part files and glue them together to output file.
        All args are filenames.
        """
        with open(outfile, 'w') as out:
            for part in sorted(parts):
                line = None
                with open(part, 'r') as pf:
                    for line in pf:
                        out.write(line)
                # make sure we have trailing endline
                if line is not None and not line.endswith(os.linesep):
                    out.write(os.linesep)

    def _handle_output(self, name, output, target, croptxt=False):
        """
        Handle sigle target location, as passed to the pig script.

        :param name: name of the param, mostly for logging purposes
        :param target: desired path to store glued file (or directory)
        :param output: directory produced by pig
        """
        def check_success_flag():
            """
            This check is only required for pig-generated final directories
            but not for custom multi-dirs.
            """
            if not op.exists(op.join(output, '_SUCCESS')):
                raise ValueError(
                    'Output directory {} exists but has no _SUCCESS flag - '
                    'probably pig failed?'.format(output))

        logger.info('Handling output: n %s, o %s, t %s', name, output, target)
        if not op.exists(output):
            logger.error('Output dir already exists!')
            raise ValueError(
                'Expected target directory {} '
                'was not created by pig. '
                'Are the params correct?'.format(output))

        if op.exists(target):
            raise ValueError(
                'Output file {} already exists'.format(target))

        # now we want to check if they created several nested files
        parts = glob(op.join(output, 'part-*'))
        if parts:
            # this is a regular target, we should join it to a single file
            check_success_flag()
            logger.info('%s is a single target, glueing to %s',
                        name, op.basename(target))
            self._glue_parts(parts, target)
            return

        # maybe it is a MultiStorage?
        subdirs = [
            # remove trailing slash which breaks basename()
            d.rstrip(op.sep)
            for d in
            glob(op.join(output, '*', ''))  # with trailing shash -> dirs
        ]
        # vvv this complex expression means
        # "each subdir should have at least one file
        # with name starting with subdir's name",
        # because that is how pig names them
        if subdirs and all(
            glob(op.join(
                subdir,
                # use abspath to avoid trailing slash,
                # or else basename returns empty string
                '{}-*'.format(op.basename(subdir)),
            ))
            for subdir in subdirs
        ):
            check_success_flag()

            # XXX: originally we silently removed .txt suffix if any
            # from the target name, but if we do that
            # then it leads to problems with handler not expecting this.
            # Instead, if handler expects a directory
            # then it will just not pass 'something.txt' as an argument;
            # and if it actually passed something like this
            # then we should consider that intentional, right?
            # (but what to do with default output name?)
            if croptxt and target.endswith('.txt'):
                target = target[:-4]
            # But it is good to crop it when dealing with subdirs;
            # so let's make it configurable.

            logger.info('%s is a MultiStorage, glueing to %s/*.txt...',
                        name, op.basename(target))
            # in this case target should become a directory
            os.mkdir(target)
            for subdir in subdirs:
                subparts = glob(
                    op.join(subdir, '{}-*'.format(op.basename(subdir))))
                subout = op.join(target, op.basename(subdir) + '.txt')
                self._glue_parts(subparts, subout)
            return

        # Not a MultiStorage but still multiple dirs
        # it means that pig script used construct like '$output/dir1' etc
        # so let's call ourselves recursively
        elif subdirs:
            # don't check success flag here
            if croptxt and target.endswith('.txt'):
                target = target[:-4]
            os.mkdir(target)
            for subdir in subdirs:
                base = op.basename(subdir)
                self._handle_output(
                    '{}/{}'.format(name, base),
                    subdir,
                    op.join(target, base + '.txt'),
                    croptxt=True,  # if it turns out to become dir then crop
                )
            return

        else:
            raise ValueError(
                '{} is neither single nor subdir - '
                'errored target? Path is {}'.format(name, output))

    def run(self, script, infile=None, outfile=None):
        """
        Run pig script ``script``.

        Input and output filenames will be passed as params to the script.
        Input files should already exist at the time of running this method.

        Both ``infile`` and ``outfile`` arguments
        may accept one of the 3 value types:

        1. String - name of the file in `INDIR` or `OUTDIR`, without path;
           will be passed to the script as ``input`` or ``output`` param.
        2. List of filenames - each of them will be passed as numbered param:
           ``input1``, ``input2``..., ``output1``, ``output2``...
        3. Dict of filenames - each of them will be passed as named param:
           if ``infile`` is
           ``{'address': 'addresses.txt', 'phone': 'phones.txt'}``
           then params will be as follows:
           ``input_address=addresses.txt``, ``input_phone=phones.txt``.

        Result(s) of script execution will be saved in `OUTDIR`.
        For each outfile passed, the script is expected to create a directory
        containing ``part*`` files for corresponding data set.
        We will then merge these parts into files with given names.

        :param script: pig script filename
            (with or without ``.pig`` extension),
            path should be relative to the root of `PIG_SCRIPTS_BUCKET`.

        :param infile: name of input data file (or files).
            Defaults to scriptname with extension replaced with `.txt`.
            Name should not include path, the file is looked up in INDIR.

        :param outfile: name of the file to write output to,
            Defaults to scriptname with extension replaced with '.txt'.
            Name should not include path, the file will be created in OUTDIR.
        """
        if not self.ready:
            raise ValueError('PigSession is not ready, you forgot ensure_ready')

        # avoid leading slashes as they lead to strange consequences
        script = script.lstrip('/')
        if not script.endswith('.pig'):
            script += '.pig'
        # We download script on each launch.
        # In late production we will just put a directory with all scripts
        # to lambda package
        # and give it the same name as the bucket -
        # then get_s3 will return LocalS3 which actually just copies file
        # from one place to another within local filesystem.
        localscript = op.join(self.directory, op.basename(script))
        self._download_script(script, localscript)

        inparams = self._filelist_to_params(script, 'input', infile)
        outparams = self._filelist_to_params(script, 'output', outfile)
        # pig wants output to be directories,
        # so we will change names to avoid clash when we glue these files
        outdirs = {k: v+'.dir' for k, v in outparams.items()}
        addparams = {
            'pigdir': self.FULLPIGDIR,
        }
        params = dict(chain(*(p.items() for p in (
            inparams, outdirs, addparams))))

        logger.info('Will run pig with these params: %s', params)

        # run pig
        self._run_pig(params, localscript)

        # join output files
        logger.info('Pig finished, now handling its output')
        for name, outpath in outparams.items():
            odir = outpath + '.dir'
            self._handle_output(name, odir, outpath)

    def __enter__(self):
        return self

    def __exit__(self, exc, value, tb):
        self.cleanup()

    def rotate_directories(self, *dirs):
        """
        Take a list of directories (each of them should contain just files)
        and re-group them by file names,
        like python's :func:`zip` does with sequence indices
        or reversing python's `dict` with
        ``{val: key for key, val in mydict.items()}``.
        Filename extensions are left the same as original.

        For example, given original directory structure::

            phones/
                id125.txt
                id127.txt
                id128.txt
            addresses/
                id121.txt
                id127.txt

        Original directories will be removed
        and resulting structure will be created::

            id121/
                addresses.txt
            id125/
                phones.txt
            id127/
                phones.txt
                addresses.txt
            id128/
                phones.txt

        :param dirs: directories to handle.
            All directories are expected to be just names (not paths)
            within `.OUTDIR`.
        """
        rotate_directories(self.OUTDIR, *dirs)

    def cleanup(self):
        """
        Remove temporary working directory;
        after calling this method, this instance should never be used again.

        This will also be called when leaving context-managed block
        or when destroying the class.
        """
        if not self.keep_files:
            rmtree_if_exists(self.directory)
        # if we were specially requested to keep it for debugging purposes
        # then obey.
        self._clean = True

    def __del__(self):
        if not self._clean:
            self.cleanup()
            warnings.warn('PigSession was not properly closed')

def get_pig(*args, **kwargs):
    """
    Obtain a new pig session instance, with separate input/output directory
    """
    return PigSession(*args, **kwargs)
