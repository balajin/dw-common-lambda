# The purpose of this module is to test both local and real S3
# using the same testing code.
# Only fixture will change.

import os

import pytest

from .conftest import importextra
get_s3 = importextra('s3', 'get_s3')


@pytest.fixture(params=['local', 'real'])
def s3_type(request):
    return request.param

@pytest.fixture
def make_s3(moto_s3, tmpdir_factory):
    bucket_idx = [0]  # like nonlocal
    def make(s3_type):
        if s3_type == 'local':
            tdir = tmpdir_factory.mktemp(basename='bucket')
            return get_s3(tdir.strpath, force_local=True)
        else:
            # first create bucket
            # we want the name to be unique,
            # for multiple simulatneous buckets to work properly
            bucket_idx[0] += 1
            bucket = 'test_bucket_{}'.format(bucket_idx[0])
            moto_s3.Bucket(bucket).create()
            return get_s3(bucket, force_real=True)
    return make

@pytest.fixture
def s3(s3_type, make_s3):
    return make_s3(s3_type)


def test_empty(s3, tmpdir):
    # check that nothing exists
    assert list(s3) == []
    # check that exceptions are properly raised
    with pytest.raises(Exception):
        s3.download('file1', tmpdir.strpath)
    with pytest.raises(Exception):
        s3.download('/path/to/file2', tmpdir.strpath)
    with pytest.raises(Exception):
        s3.fetch('another/file')
    assert s3.exists('whatever') is False
    assert s3.exists('path/to/whatever') is False


def test_listing(s3, tmpdir):
    # check that nothing exists
    assert list(s3) == []
    # another api
    assert list(s3.list()) == []

    # create some stuff
    tmpdir.join('file1.txt').write('hello')
    tmpdir.join('file2').write('abc')
    tmpdir.join('dir1', 'subfile.ext').write('x', ensure=True)
    tmpdir.join('dir1', 'other').write('y')
    tmpdir.join('dir2', 'z').write('z', ensure=True)

    # upload directory
    s3.upload(tmpdir.strpath, 'base')
    # and a separate file
    s3.upload(tmpdir.join('file1.txt').strpath)

    # and assert that it is listed correctly
    assert sorted(s3.list()) == sorted(s3) == [
        'base/dir1/other',
        'base/dir1/subfile.ext',
        'base/dir2/z',
        'base/file1.txt',
        'base/file2',
        'file1.txt',
    ]

    # also test explicit prefix
    # (any combination of leading or trailing slashes should be handled)
    for prefix in ['base', '/base', '/base/', 'base//']:
        assert sorted(s3.list(prefix=prefix)) == [
            'base/dir1/other',
            'base/dir1/subfile.ext',
            'base/dir2/z',
            'base/file1.txt',
            'base/file2',
        ]

    assert sorted(s3.list_dirs()) == ['base/']
    assert sorted(s3.list_dirs('')) == ['base/']
    assert sorted(s3.list_dirs('/')) == ['base/']
    assert sorted(s3.list_dirs('base')) == [
        'base/dir1/',
        'base/dir2/',
    ]

    # partial prefix should not work
    assert list(s3.list('/base/dir')) == []

    # subdirectory should be handled correctly
    assert sorted(s3.list('/base/dir1/')) == [
            'base/dir1/other',
            'base/dir1/subfile.ext',
    ]

    # test partial lists
    unsorted = list(s3)
    assert len(unsorted) == 6
    assert list(s3.list(only_after=unsorted[3])) == unsorted[4:]
    # TODO more tests, e.g. for prefix+only_after


def test_file_upload_download_fetch(s3, tmpdir):
    src1 = tmpdir.join('myfile.txt')
    src1.write('Hello World\nabc')
    dst1 = tmpdir.join('otherfile.txt')

    # use original name for uploading...
    s3.upload(src1.strpath)
    s3.download('myfile.txt', dst1.strpath)

    assert src1.exists()  # was not removed
    assert src1.read() == 'Hello World\nabc'  # was not damaged
    assert dst1.exists()
    assert dst1.read() == 'Hello World\nabc'

    # now binary file and specify name on uploading
    src2 = tmpdir.join('thefile.bin')
    content = os.urandom(1024)
    src2.write_binary(content)
    dst2 = tmpdir.join('mega.bin')

    assert s3.exists('path/to/mega.bin') is False

    s3.upload(src2.strpath, 'path/to/mega.bin')
    assert s3.exists('path/to/mega.bin') is True

    # download to directory using basename from key
    s3.download('path/to/mega.bin', tmpdir.strpath + '/')

    assert dst2.exists()
    assert dst2.read_binary() == content

    # also test fetching
    assert s3.fetch('myfile.txt') == 'Hello World\nabc'
    assert s3.fetch('path/to/mega.bin', binary=True) == content

    # and iterating over open_r result
    assert list(s3.open_r('myfile.txt')) == [
        'Hello World\n', 'abc']
    assert list(s3.open_r('myfile.txt', binary=True)) == [
        b'Hello World\n', b'abc']

def test_upload_fobj(s3, tmpdir):
    src = tmpdir.join('myfile')
    src.write('Hello World')
    dst = tmpdir.join('destination file')

    with src.open('rb') as sobj:
        # filename should be properly calculated
        s3.upload(sobj, 'prefix/')
    s3.download('prefix/myfile', dst.strpath)

    assert dst.exists()
    assert dst.read() == 'Hello World'

def test_upload_custom_fobj(s3, tmpdir):
    # it is unnamed!
    class Fobj(object):
        def __init__(self, content):
            self.content = content.encode()

        def read(self, size=None):
            if size is None:
                ret = self.content
                self.content = ''
            else:
                ret = self.content[:size]
                self.content = self.content[size:]
            return ret

    src = Fobj('Hello World')
    dst = tmpdir.join('tgt')

    # name should be mandatory in these cases
    with pytest.raises(ValueError, match='has no name'):
        s3.upload(src)
    with pytest.raises(ValueError, match='has no name'):
        s3.upload(src, 'path/to/')

    # this should work (and fobj should not yet be consumed)
    s3.upload(src, 'path/name')
    s3.download('path/name', dst.strpath)

    assert dst.exists()
    assert dst.read() == 'Hello World'

def test_removing(s3, tmpdir):
    src = tmpdir.join('file.txt')
    src.write('Hello World')
    dst = tmpdir.join('result.txt')

    s3.upload(src.strpath, 'directory/filename.txt')
    s3.download('directory/filename.txt', dst.strpath)

    assert dst.exists()
    assert dst.read() == 'Hello World'

    # now remove it
    s3.remove('directory/filename.txt')
    with pytest.raises(Exception):
        s3.download('directory/filename.txt', dst.strpath)

    # check that 'directory' is also free
    s3.upload(src.strpath, 'directory')

def test_copy_to(s3, s3_type, make_s3, tmpdir):
    src = tmpdir.join('file.txt')
    src.write('Hello World', ensure=True)
    dst = tmpdir.join('result.txt')

    key1 = 'dir1/dir2/srcfile.txt'
    key2 = 'somedir/somefile.txt'

    s3.upload(src.strpath, key1)
    # within the same bucket...
    s3.copy_to(key1, to_key=key2)
    s3.download(key2, dst.strpath)

    assert dst.read() == 'Hello World'
    dst.remove()

    # no-param test
    with pytest.raises(ValueError):
        s3.copy_to(key1)

    # different bucket test
    other_s3 = make_s3(s3_type)
    # make sure such file does not exist yet on the target
    with pytest.raises(ValueError):
        other_s3.download(key1, dst.strpath)
    with pytest.raises(ValueError):
        other_s3.download(key2, dst.strpath)

    # copy
    s3.copy_to(key1, to_bucket=other_s3.bucket)
    other_s3.download(key1, dst.strpath)
    assert dst.read() == 'Hello World'
    dst.remove()

    # test passing ready bucket object to dest s3, and custom key
    s3.copy_to(key1, to_bucket=other_s3, to_key=key2)
    other_s3.download(key2, dst.strpath)
    assert dst.read() == 'Hello World'
    dst.remove()

    # test that mixed types won't work
    wrong_s3 = make_s3('local' if s3_type == 'real' else 'real')
    with pytest.raises(ValueError):
        s3.copy_to(key1, to_bucket=wrong_s3)
