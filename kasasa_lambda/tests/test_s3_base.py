import pytest

from .conftest import importextra
BaseS3Helper, get_s3, LocalS3, RealS3 = importextra('s3', (
    'BaseS3Helper', 'get_s3', 'LocalS3', 'RealS3',
))


def test_get_s3(mocker):
    # avoid any real requests
    mocker.patch('boto3.resource')
    mocker.patch('boto3.client')

    s3 = get_s3('.')
    assert s3
    assert isinstance(s3, LocalS3)

    s3 = get_s3('.', force_real=True)
    assert isinstance(s3, RealS3)

    s3 = get_s3('.', force_local=True)
    assert isinstance(s3, LocalS3)

    # now check some not-existing-localy name
    # (remember, all RealS3 buckets are considered valid for this test)
    s3 = get_s3('real-production-super-important-bucket')
    assert isinstance(s3, RealS3)

    # and for the sake of completeness...
    with pytest.raises(ValueError):
        get_s3('anything', force_real=True, force_local=True)


@pytest.fixture
def bs3():
    return BaseS3Helper('bucket')

def test_download(bs3, mocker):
    bs3._download = mocker.stub()

    key = 'my_dir/my_object.txt'

    bs3.download(key, '/tmp/')
    bs3._download.assert_called_with(key, '/tmp/my_object.txt')

    with pytest.raises(ValueError):
        bs3.download(key, '/tmp/no-such-dir/')

    bs3.download(key, '/tmp/outfile.dat')
    bs3._download.assert_called_with(key, '/tmp/outfile.dat')

def test_upload(bs3, mocker, tmpdir):
    bs3._upload = mocker.stub()

    # empty file should not be accepted
    with pytest.raises(ValueError):
        bs3.upload('/tmp/nosuchfile')

    # but directory is okay
    bs3.upload('/tmp')

    # for empty file it should not have been called
    assert bs3._upload.call_count == 1

    myfile = tmpdir.join('myfile.txt')
    with myfile.open('w') as f:
        f.write('hello world')
    assert myfile.exists()

    bs3.upload(myfile.strpath)
    bs3._upload.assert_called_with(
        myfile.strpath, 'myfile.txt', False)

    bs3.upload(myfile.strpath, 'path/to/another.file')
    bs3._upload.assert_called_with(
        myfile.strpath, 'path/to/another.file', False)

    bs3.upload(myfile.strpath, 'somedir/')
    bs3._upload.assert_called_with(
        myfile.strpath, 'somedir/' + myfile.basename, False)

def test_iter(bs3, mocker):
    bs3.list = mocker.MagicMock(side_effect=lambda: iter([1, 2, 3]))

    assert list(bs3) == [1, 2, 3]
    assert bs3.list.call_count == 1

def test_not_implemented_raised(bs3):
    # just for coverage

    with pytest.raises(NotImplementedError):
        bs3.download('the_file.txt', '/tmp/')

    with pytest.raises(NotImplementedError):
        bs3.upload(__file__)

    with pytest.raises(NotImplementedError):
        list(bs3)
