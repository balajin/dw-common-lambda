# Test db fixture itself
import pytest

from .conftest import importmodule

# if it is not available then we may want to skip tests in this module
pymysql = importmodule('pymysql')


# use dbfixture plugin for this module only
pytest_plugins = 'kasasa_lambda.tests.dbfixture'


def test_dbfixture_basic(db):
    conn = pymysql.connect(host='thehost', port=42, database='thedb')
    # check that connect was overriden
    assert pymysql.connect is db.connect
    assert not isinstance(conn, pymysql.cursors.Cursor)

    # check that fake cursor is returned using `with` stmt...
    with conn as cur:
        assert hasattr(cur, 'execute')
    # ...and using explicit cursor() method
    assert hasattr(conn.cursor(), 'execute')

    # try to execute some query - which should fail as there is no mock for it
    with pytest.raises(Exception, match='Could not find matching mock'):
        with conn as cur:
            cur.execute('random query')

    # now add mock
    assert hasattr(db, 'on')
    db.on('select.*from.*persons', [
        ('john', 'smith', 23),
        ('alice', 'johnsons', 18),
    ])
    # and test that it works
    with conn as cur:
        # case should be ignored
        ret = cur.execute('abc SELECT * FROM persons WHERE q1 = q2')
        assert ret == 2  # number of rows returned (FIXME right?)
        records = list(cur)
        assert len(records) == 2
        assert records[0] == ('john', 'smith', 23)
    # non-matching queries should still fail
    with pytest.raises(Exception, match='Could not find matching mock'):
        with conn as cur:
            cur.execute('this query wont match')

    # params should be substituted before matching
    with conn as cur:
        cur.execute('SELECT * FROM %s', 'persons')
