# This fixture is not used in common lib tests
# but is useful for some lambdas,
# hence it is provided by common lib.
# Enable it by adding the following line to your conftest.py or whatever:
# pytest_plugins = 'kasasa_lambda.tests.dbfixture'

import re

import pytest
try:
    import pymysql
except ImportError:
    # we use psycopg2 or something similar
    pymysql = None


class FakeCursor(object):
    real_init = (
        pymysql.connections.Connection.__init__
        if pymysql else None
    )
    if hasattr(real_init, 'im_func'):  # python2 - need to unwrap
        real_init = real_init.im_func
    # avoid python converting it to instance method of FakeCursor
    real_init = staticmethod(real_init)

    def __init__(self):
        self.queries = []
        self._last_result = ()  # empty set
        self.connection = None
        self.on_connected = None

    def on(self, querymatch, result, description=None):
        self.queries.append(
            (
                re.compile(querymatch, re.I | re.MULTILINE | re.DOTALL),
                result,
                description,
            ),
        )

    def execute(self, query, *params):
        query_tpl = query
        if params:
            query = query % params

        for tpl, result, description in self.queries:
            if tpl.search(query):
                break
        else:
            raise ValueError(
                'Could not find matching mock for query: %s' % query)

        if callable(result):
            result = result(query_tpl, *params)

        if isinstance(result, Exception):
            # we were requested to fail here; do exactly that,
            # without updating previous result (if any)
            raise result

        self._last_result = result or ()  # None or False means empty set
        self.description = description
        return len(self._last_result)

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        pass  # TODO?

    def __iter__(self):
        return iter(self._last_result)

    # add other accessor methods when required

@pytest.fixture
def db(mocker):
    """
    Very simple database connection fixture
    suitable for most trivial cases only.

    Usage: when setting up, call `db.on(queryTemplate, result)`.
    If query matches the template (which is compiled as case-insensitive regex),
    we will return the data you passed as query's result.
    If result is an exception then it will be raised on query execution.
    If result is callable then it is called with execute() arguments,
    and what it returns will be used.
    """
    fake_cursor = FakeCursor()

    def make_connection(*args, **kwargs):
        conn = mocker.MagicMock()
        conn.__enter__.return_value = fake_cursor
        conn.cursor.return_value = fake_cursor
        fake_cursor.connection = conn

        # call the constructor to parse and validate all parameters
        fake_cursor.real_init(conn, *args, **kwargs)

        if fake_cursor.on_connected:
            # make it possible to assert on database name
            fake_cursor.on_connected(conn)

        return conn

    if pymysql:
        connect = mocker.patch('pymysql.connect')
    else:
        connect = mocker.MagicMock()
    connect.side_effect = make_connection
    # for called_with assertions
    fake_cursor.connect = connect

    return fake_cursor
