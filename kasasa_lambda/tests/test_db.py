import pytest

from .conftest import importextra, importmodule
get_db_connection, parse_db_envvars = importextra(
    'db', 'get_db_connection', 'parse_db_envvars')


# Some of these tests require pymysql, but not all of them


def test_url_required(monkeypatch):
    importmodule('pymysql')  # skip or fail if it is not available

    # empty value is not valid
    with pytest.raises(ValueError):
        get_db_connection('')

@pytest.mark.parametrize(
    'data', [
        ('mysql://JohnDoe:Secret@88.22.33.11:1324/great_db?charset=utf8',
         1324),
        ('mysql://JohnDoe:Secret@88.22.33.11/great_db?charset=utf8',
         3306),  # that is the default mysql port
    ])
def test_get_db_connection(monkeypatch, mocker, caplog, data):
    importmodule('pymysql')  # skip or fail if it is not available

    url, port = data

    # Don't really connect
    mocker.patch('pymysql.connections.Connection.connect')

    conn = get_db_connection(url)

    assert conn
    assert conn.connect.call_count == 1
    assert conn.user == 'JohnDoe'
    # pymysql in py3 converts password to bytes
    assert conn.password == b'Secret'
    assert conn.host == '88.22.33.11'
    assert conn.port == port
    assert conn.db == 'great_db'
    assert conn.charset == 'utf8'

    assert '88.22.33.11' in caplog.text
    assert 'Secret' not in caplog.text


def test_get_db_connection_by_params(mocker, caplog):
    importmodule('pymysql')  # skip or fail if it is not available

    # Don't really connect
    mocker.patch('pymysql.connections.Connection.connect')

    conn = get_db_connection(
        user='JohnDoe',
        password='Secret',
        host='88.22.33.11',
        port=1234,
        database='great_db',
        charset='utf8',
    )

    assert conn
    assert conn.connect.call_count == 1
    assert conn.user == 'JohnDoe'
    assert conn.password == b'Secret'
    assert conn.host == '88.22.33.11'
    assert conn.port == 1234
    assert conn.db == 'great_db'
    assert conn.charset == 'utf8'


def test_parse_db_envvars(monkeypatch, tmpdir):
    _env = {}  # the dict which we will edit to simulate environment
    monkeypatch.setattr('os.environ', _env)

    # helper func
    def env(**kw):
        _env.clear()
        _env.update(kw)

    env()  # empty environment
    with pytest.raises(KeyError,
                       match='None of.*DATABASE_HOST.*HOST.*was found'):
        parse_db_envvars()

    # try with URL
    env(DATABASE_URL='mysql://user:pass@host:123/dbname')
    assert parse_db_envvars() == {
        'user': 'user',
        'password': 'pass',
        'host': 'host',
        'port': 123,
        'database': 'dbname',
    }

    # try with prefixed vars and no port
    env(
        DATABASE_HOST='host',
        DATABASE_USERNAME='user',
        DATABASE_PASSWORD='pass',
        DATABASE='dbname',
    )
    assert parse_db_envvars() == {
        'host': 'host',
        'user': 'user',
        'password': 'pass',
        'database': 'dbname',
    }

    # same but with custom port
    env(
        DATABASE_HOST='host',
        DATABASE_PORT='3128',  # env vars are always strings
        DATABASE_USERNAME='user',
        DATABASE_PASSWORD='pass',
        DATABASE='dbname',
    )
    assert parse_db_envvars() == {
        'host': 'host',
        'port': 3128,
        'user': 'user',
        'password': 'pass',
        'database': 'dbname',
    }

    # unprefixed vars
    env(
        HOST='host',
        PORT='3121',
        USERNAME='john',
        PASSWORD='smith',
        DATABASE='dbname',
    )
    assert parse_db_envvars() == {
        'host': 'host',
        'port': 3121,
        'user': 'john',
        'password': 'smith',
        'database': 'dbname',
    }

    # now with no database specification
    env(
        HOST='host',
        USERNAME='user',
        PASSWORD='pass',
    )
    with pytest.raises(KeyError, match='DATABASE not specified'):
        parse_db_envvars()
    # but if we allow missing db...
    for db in False, None:
        print(db)
        assert parse_db_envvars(database=db) == {
            'host': 'host',
            'user': 'user',
            'password': 'pass',
        }

    # test different password obtaining ways
    # plaintext env var - already tested
    # in url - already tested
    # test separate vars with no password
    env(
        HOST='host',
        USERNAME='user',
        DATABASE='db',
    )
    with pytest.raises(KeyError, match='Could not find database password'):
        parse_db_envvars()

    # test explicitly passed secrets
    env(
        HOST='host',
        USERNAME='user',
        DATABASE='db',
        DATABASE_PASSWORD_KEY='my_db_password',
    )
    assert parse_db_envvars(secrets={
        'x': 'y',
        'my_db_password': 'thepass',
    }) == {
        'host': 'host',
        'user': 'user',
        'database': 'db',
        'password': 'thepass',
    }

    # test implicitly passed secrets and url
    tmpdir.join('mysecret.json').write('{"db_password":"thePassword"}')
    env(
        DATABASE_URL='mydb://user:pass@host:123/dbname',
        DATABASE_PASSWORD_KEY='db_password',
        SECRETS_BUCKET=tmpdir.strpath,
        SECRETS_KEY='mysecret.json',
    )
    assert parse_db_envvars() == {
        'user': 'user',
        'password': 'thePassword',  # URL's one is ignored
        'host': 'host',
        'port': 123,
        'database': 'dbname',
    }
