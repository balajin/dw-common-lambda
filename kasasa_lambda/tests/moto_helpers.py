"""
Common moto-related fixtures to be used in external tests as well as here.
"""

from json import loads as json_loads
import os

import boto3
from moto import mock_s3, mock_lambda, mock_sns, mock_sqs
import pytest


@pytest.fixture
def moto_credentials(monkeypatch):
    # make sure some credentials are set (although we don't actually use them),
    # or else boto will fail - even when mocket by moto.
    for var, val in {
        'AWS_ACCESS_KEY_ID': 'the_key',
        'AWS_SECRET_ACCESS_KEY': 'the_secret',
        'AWS_DEFAULT_REGION': 'us-west-2',
    }.items():
        if not os.environ.get(var):
            monkeypatch.setenv(var, val)


@pytest.fixture(autouse=True)
def moto_s3(moto_credentials):
    with mock_s3():
        # for convenience
        yield boto3.resource('s3')


@pytest.fixture(autouse=True)
def moto_lambda(moto_credentials):
    with mock_lambda():
        yield boto3.client('lambda')


@pytest.fixture
def moto_sqs(moto_credentials):
    with mock_sqs():
        yield boto3.resource('sqs')


@pytest.fixture
def moto_sqs_queue(moto_sqs):
    queue = moto_sqs.create_queue(QueueName='test_queue')
    return queue


@pytest.fixture
def moto_sns(moto_credentials):
    # TODO allow intercepting messages via SQS subscription
    with mock_sns():
        yield boto3.resource('sns')


@pytest.fixture
def moto_sns_topic(moto_sns, moto_sqs_queue):
    topic = moto_sns.create_topic(Name='test_topic')
    topic.subscribe(
        Protocol='sqs',
        Endpoint=moto_sqs_queue.attributes['QueueArn'],
    )
    return topic


class MotoSnsFixture(object):
    """
    Helper class to simplify SNS testing.
    Incapsulates SNS topic and SQS queue subscribed to it
    and supports for easy retrieval of messages
    which were published to the topic.
    """
    def __init__(self, topic, queue, use_json=True):
        self.topic = topic
        self.queue = queue
        self.use_json = use_json

    def get_next_published_message(self, json=None):
        if json is None:
            json = self.use_json

        messages = self.queue.receive_messages(
            MaxNumberOfMessages=1,
            WaitTimeSeconds=1,
        )
        if not messages:
            return
        msg = messages[0]
        sqs_data = json_loads(msg.body)
        body = sqs_data['Message']
        if json:
            return json_loads(body)
        else:
            return body

    def get_published_messages(self, json=None):
        while True:
            m = self.get_next_published_message(json)
            if m is None:
                return
            yield m

    @property
    def messages(self):
        # just a shorthand,
        # useful for testing
        return list(self.get_published_messages())


@pytest.fixture
def moto_sns_helper(moto_sns_topic, moto_sqs_queue):
    """
    Usage::

        topic_arn = moto_sns_helper.topic.arn
        # TODO publish some JSON stuff to the topic...
        assert moto_sns_helper.messages == [
            {'message1': 'data'},
            {'data_of': 'message_2'},
            # ...
        ]
    """
    return MotoSnsFixture(moto_sns_topic, moto_sqs_queue)
