SET debug 'on'

A = LOAD '$input' USING PigStorage(',') AS (f1,f2);

STORE A INTO '$output' USING PigStorage(',');
