import json
import os.path as op
import os

import botocore
import pytest

from .conftest import importextra
get_s3, RealS3 = importextra('s3', 'get_s3', 'RealS3')


@pytest.fixture
def s3(mocker):
    # make sure there will be no real requests
    mocker.patch('boto3.resource')
    mocker.patch('boto3.client')
    return get_s3('bucket', force_real=True)


def test_real_s3_creation(s3):
    s3.S3.meta.client.head_bucket.side_effect = botocore.client.ClientError(
        {'Error': {
            'Code': '404',
            'Message': 'everything broken',
        }}, 123)

    with pytest.raises(ValueError):
        # instantiate it again
        RealS3('anybucket')

def test_real_s3_list(s3, mocker):
    s3.bucket_obj.objects.all.return_value = [
        mocker.MagicMock(key=i) for i in range(5)]

    objs = list(s3.list())
    assert objs == list(range(5))

def test_real_s3_download(s3, tmpdir):
    s3.download('bestfile.txt', tmpdir.strpath + op.sep)
    assert s3.bucket_obj.download_file.call_count == 1
    s3.bucket_obj.download_file.assert_called_with(
        'bestfile.txt',
        tmpdir.join('bestfile.txt').strpath,
    )

def test_real_s3_upload(s3, tmpdir, mocker):
    # first test file uploading
    src = tmpdir.join('ourfile.txt')
    src.write('hello world')

    # test "overwrite" check
    # suppose such object already exists
    s3.bucket_obj.objects.filter.return_value = [
        mocker.MagicMock(key=src.basename),
    ]
    with pytest.raises(ValueError):
        s3.upload(src.strpath)
    # or suppose it is a directory
    s3.bucket_obj.objects.filter.return_value = [
        mocker.MagicMock(key=src.basename + '/some/file.txt'),
    ]
    with pytest.raises(ValueError):
        s3.upload(src.strpath)
    # revert return value back
    s3.bucket_obj.objects.filter.return_value = mocker.MagicMock()

    s3.upload(src.strpath)
    assert s3.bucket_obj.upload_file.call_count == 1
    s3.bucket_obj.upload_file.assert_called_with(
        src.strpath, 'ourfile.txt',
        ExtraArgs=s3.upload_extra_args,
    )

    # test directory uploading
    src = tmpdir.join('mydir')
    src.join('somefile.txt').write('some content', ensure=True)
    src.join('otherfile.txt').write('another content', ensure=True)
    s3.upload(src.strpath)
    s3.bucket_obj.upload_file.assert_any_call(
        src.join('somefile.txt').strpath,
        'mydir/somefile.txt',
        ExtraArgs=s3.upload_extra_args,
    )
    s3.bucket_obj.upload_file.assert_any_call(
        src.join('otherfile.txt').strpath,
        'mydir/otherfile.txt',
        ExtraArgs=s3.upload_extra_args,
    )


# localstack/moto tests
def test_moto_s3_roundtrip(moto_s3, tmpdir):
    # ensure bucket is created
    moto_s3.Bucket('example-bucket').create()

    # We don't use our s3 fixture here becasue we don't want to monkeypatch
    s3 = get_s3('example-bucket')
    assert isinstance(s3, RealS3)

    src = tmpdir.join('megafile.bin')
    content = os.urandom(300)
    src.write_binary(content)
    dst = tmpdir.join('outfile.bin')

    s3.upload(src.strpath, 'wherever/mfile.bin')
    s3.download('wherever/mfile.bin', dst.strpath)

    assert dst.exists()
    assert dst.read_binary() == content

    # Also test directory uploading
    src = tmpdir.join('mydir')
    src.join('file1.txt').write('hello', ensure=True)
    src.join('file2.txt').write('world')
    dst = tmpdir.join('outdir')

    s3.upload(src.strpath, 'mydir')
    s3.download('mydir', dst.strpath)

    assert dst.exists()
    assert dst.join('file1.txt').read() == 'hello'
    assert dst.join('file2.txt').read() == 'world'

    # Test directory existence checking
    with pytest.raises(ValueError, match="Won't overwrite"):
        s3.upload(src.strpath, 'mydir', overwrite=False)

    # ... and overwriting
    src = tmpdir.join('newfile')
    src.write('important data')
    with pytest.raises(ValueError, match="Won't overwrite"):
        # check that file won't overwrite directory without permission
        s3.upload(src.strpath, 'mydir')
    # and will do when requested
    s3.upload(src.strpath, 'mydir', overwrite=True)

    dst = tmpdir.join('gotfile')
    s3.download('mydir', dst.strpath)
    assert dst.exists()
    assert dst.isfile()
    assert dst.read() == 'important data'


@pytest.mark.xfail(reason='looks like moto does not support policies')
def test_upload_extra_args(moto_s3, tmpdir):
    # create bucket
    bucket = moto_s3.Bucket('test-bucket')
    bucket.create()

    # set policy to that bucket to "prohibit non-encrypted"
    # http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingServerSideEncryption.html
    bucket.Policy().put(Policy=json.dumps(dict(
        Version='2012-10-17',
        Id='PutObjPolicy',
        Statement=[
            dict(
                Effect='Deny',
                Principal='*',
                Action='s3:PutObject',
                Resource='arn:aws:s3:::{name}/*'.format(name=bucket.name),
                **add
            ) for add in (dict(
                Sid='DenyIncorrectEncryptionHeader',
                Condition=dict(
                    StringNotEquals={
                        's3:x-amz-server-side-encryption': 'AES256',
                    },
                ),
            ), dict(
                Sid='DenyUnEncryptedObjectUploads',
                Condition=dict(
                    Null={
                        's3:x-amz-server-side-encryption': 'true',
                    }
                ),
            ))
        ],
    )))

    # TODO check upload, upload_fobj, upload_dir and rename
    s3 = get_s3(bucket.name)
    assert isinstance(s3, RealS3)

    src = tmpdir.join('file.txt')
    src.write('hello')

    # upload without encryption request should fail
    with pytest.raises(Exception):
        bucket.upload_file(src.strpath, 'somename.txt')

    # and this should work
    s3.upload(src.strpath)
