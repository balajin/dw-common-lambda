import pytest


pytest_plugins = [
    'kasasa_lambda.tests.moto_helpers',
]


def pytest_addoption(parser):
    try:
        parser.addoption('--quick', action='store_true', help='skip slow tests')

        parser.addoption('--all-extras', action='store_true',
                         help='force testing all extras, '
                         'rather than skipping ones with unmet dependencies')
        parser.addoption('--extras-optional',
                         dest='all_extras', action='store_false',
                         help='revert --all-extras option')
    except ValueError:  # already added in ../../conftest.py?
        pass  # okay

slow = pytest.mark.skipif('config.getoption("--quick")',
                          reason='slow test disabled because of --quick option')


def importmodule(what, msg=None):
    should_raise = None
    try:
        return __import__(what)
    except ImportError:
        if pytest.config.getoption('--all-extras'):
            print('ERROR: ImportError during importing {}; '
                  'probably corresponding extra was not installed. '
                  'Failing due to --all-extras.'.format(what))
            # would be nice to mark module as failed, but it is too hard -
            # for skipping they use a hack with allow_module_level,
            # but for failing there is not such option.
            raise
        else:
            should_raise = pytest.skip.Exception(
                msg or 'Module {} not available'.format(what),
                allow_module_level=True,
            )
    if should_raise:
        # do it here to avoid chained exceptions (as per importorskip func)
        raise should_raise


def importextra(extraname, *fromlist):
    """
    Wrapper around pytest.importorskip.
    Will use regular import (and thus throw ImportErrors)
    if --all-extras option was given.
    Also supports a kind of "from ... import ..." syntax.

    :param extraname: name of the submodule to import from kasasa_lambda;
    also expected to match the extra name.
    :param fromlist: optional part or list of parts to return from that module.
    Useful for imitating 'from kasasa_lambda.XYZ import ABC, DEF' syntax.
    """
    mod = importmodule(
        '.'.join(['kasasa_lambda', extraname]),
        msg='Extra {} not available'.format(extraname)
    )
    # __import__ returns "parent" module, so we want to fetch child
    mod = getattr(mod, extraname)

    if fromlist:
        if len(fromlist) == 1 and isinstance(fromlist[0], (tuple, list)):
            # support both syntax variants
            fromlist = fromlist[0]
        parts = tuple(getattr(mod, p) for p in fromlist)
        if len(parts) == 1:
            # single item -> return it
            return parts[0]
        # multiple items -> return tuple
        return parts
    else:
        return mod


# auto-use moto_s3 and moto_lambda for our test suite
@pytest.fixture(autouse=True)
def _autouse_mock_s3_lambda(moto_s3, moto_lambda):
    pass
