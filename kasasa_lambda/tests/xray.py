import aws_xray_sdk.core
import moto
import pytest


# We don't use moto.mock_xray_client
# because (1) it is broken (it can only decorate a non-generator function)
# and (2) we are currently okay with xray sending its stuff
# to (missing) local xray daemon.
# FIXME apply mock_xray_client as an autouse fixture
# as soon as it will be possible,
# just to make sure we don't send anything to real xray
#
# XXX TODO check if moto.xray situation is fixed now

@pytest.fixture
def xray_thread_global_ctx():
    """
    Override context.
    Default implementation of XRay context uses thread local storage
    and hence sub-threads don't know about our root segment.
    This causes problems with s3transfer library
    which uses threads under the hood.
    So let's try and override context to use global variable.
    """
    # FIXME maybe there should be a better way?

    class ThreadGlobalContext(aws_xray_sdk.core.context.Context):
        def __init__(self):
            super(ThreadGlobalContext, self).__init__()
            # lambda object can be used as a simple namespace
            self._local = lambda: None

    old_context = aws_xray_sdk.core.xray_recorder.context
    aws_xray_sdk.core.xray_recorder.configure(context=ThreadGlobalContext())
    yield
    aws_xray_sdk.core.xray_recorder.context = old_context

@pytest.fixture
def xray_segment(xray_thread_global_ctx):
    """
    Make sure everything nested is running in a (fake) XRay segment.
    This is required to test any stuff which reports subsegments to xray
    but doesn't create a segment itself.
    """

    # override context.
    # Default implementation of XRay context uses thread local storage
    # and hence sub-threads don't know about our root segment.
    # This causes problems with s3transfer library
    # which uses threads under the hood.
    # So let's try and override context to use global variable.
    # FIXME maybe there should be a better way?

    class ThreadGlobalContext(aws_xray_sdk.core.context.Context):
        def __init__(self):
            super(ThreadGlobalContext, self).__init__()
            # lambda object can be used as a simple namespace
            self._local = lambda: None

    aws_xray_sdk.core.xray_recorder.configure(context=ThreadGlobalContext())

    with moto.XRaySegment() as f:
        # run the stuff
        yield f
        # probably segment.add_exception if required?..
