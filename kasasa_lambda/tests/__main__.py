"""
Tests for the library are placed within `kasasa_lambda.tests` module
and are distributed with the library.
They can be ran in two ways.

1. Standard way is to invoke `py.test` in the repository's directory.
   It will load default options from ``setup.cfg`` section and invoke all tests,
   forcing all extras to be tested.
   If some extra module has unmet requirements then its tests will fail.

2. Alternative is to launch `kasasa_lambda.tests` module bundled with the package.

This module can be used to easily run tests
for installed kasasa_lambda package.
Just run them as this::

    python -m kasasa_lambda.tests [options]

One of useful options is --quick which will skip long-running tests.

By default it will skip testing for extras which have unmet requirements;
this is to allow testing `kasasa_lambda`
as a part of deployment process for packages using only some extras.
"""
import os.path as op
import sys

import pytest

if __name__ == '__main__':
    # tests reside in the same directory as this file
    path = op.dirname(__file__)
    sys.exit(pytest.main(args=[path] + sys.argv[1:]))
