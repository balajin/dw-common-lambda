import datetime as dt
import json

import pytest

from .. import skel


def test_stats_helper():
    class Testing(object):
        @skel.collect_stats
        def meth1(self, arg=None):
            print('meth1 called. arg: %s' % arg)
            if arg == 'FAIL':
                raise ValueError('Invalid value was passed: FAIL')
            return str(arg) + ': ok'

        # should return 'foo: ok'
        def meth2(self):
            return self.meth1('foo')

        # should always fail
        def meth3(self):
            return self.meth1('FAIL')

    c = Testing()

    assert c.meth1.total == 0
    assert c.meth1.success == 0
    assert c.meth1.fail == 0
    assert c.meth1.last_exc is None

    assert c.meth1() == 'None: ok'
    assert c.meth1.success == 1

    assert c.meth2() == 'foo: ok'
    assert c.meth1.total == 2
    assert c.meth1.success == 2
    assert c.meth1.fail == 0

    with pytest.raises(ValueError, message='Invalid value was passed: FAIL'):
        c.meth3()  # should raise
    assert c.meth1.total == 3
    assert c.meth1.success == 2
    assert c.meth1.fail == 1
    assert isinstance(c.meth1.last_exc, ValueError)
    assert str(c.meth1.last_exc) == 'Invalid value was passed: FAIL'


class SimpleQueue(object):
    def __init__(self, queue):
        self.queue = queue

    def send(self, msg):
        self.queue.send_message(MessageBody=json.dumps(msg))

    @property
    def visible(self):
        return int(self.queue.attributes['ApproximateNumberOfMessages'])

    @property
    def inflight(self):
        return int(
            self.queue.attributes['ApproximateNumberOfMessagesNotVisible'])

    def __getattr__(self, name):
        """
        Proxy all properties of the underlying boto3 queue object
        """
        return getattr(self.queue, name)


class BaseSkelTest(object):
    @pytest.fixture(autouse=True)
    def queue(self, moto_sqs_queue):
        """
        Convenience wrapper for moto_sqs_queue
        """
        self.queue = SimpleQueue(moto_sqs_queue)
        return self.queue

    @pytest.fixture(autouse=True)
    def init(self, monkeypatch, queue):
        # it requires at least this env var
        monkeypatch.setenv('QUEUE_URL', queue.url)


class TestDirectRunning(BaseSkelTest):
    """
    Test basic logic, without user-implemented handlers
    """
    def test_no_messages(self):
        # run with no messages - should work fine
        skel.BaseSQSFileProcessor.main_standalone()

    @pytest.mark.parametrize('body', ('{}', '[1,2,3]', '[[[', 'nothing'))
    def test_empty_message(self, queue, body):
        # push single message - invalid one, without real data
        queue.send_message(MessageBody=body)
        # it should raise (XXX maybe better delete message and then raise?)
        with pytest.raises(ValueError, match='Got invalid message'):
            skel.BaseSQSFileProcessor.main_standalone()
        # test that the message was not removed (although it is hidden yet)
        assert queue.inflight == 1

    def test_testing_message(self, queue, caplog):
        # push test message
        queue.send({'Event': 's3:TestEvent'})
        # run the proc
        skel.BaseSQSFileProcessor.main_standalone()
        # make sure test message information was logged
        assert 'Got test message, deleting' in caplog.text
        # test that it was really deleted
        assert queue.visible == 0

    def test_real_message_fails(self, queue):
        # "Real" message should fail
        # because we didn't implement process_file method
        queue.send({'Records': [
            {
                'eventSource': 'aws:s3',
                'eventName': 'ObjectCreated:Put',
                'eventTime': '2018-02-01T12:00:00Z',
                's3': {
                    'bucket': {'name': 'test_bucket'},
                    'object': {'key': 'testfile.csv'},
                },
            }
        ]})
        with pytest.raises(NotImplementedError):
            skel.BaseSQSFileProcessor.main_standalone()

    # TODO test other entry points, not just standalone one


class TestRealLife(BaseSkelTest):
    def check(self, *messages):
        processed = []
        class MyTestingFileProcessor(skel.BaseSQSFileProcessor):
            def process_file(self, bucket, key, timestamp):
                processed.append((bucket, key, timestamp))

        for m in messages:
            self.queue.send(m)
        MyTestingFileProcessor.main_standalone()
        return processed

    def test_simple_one_message(self, queue):
        calls = self.check({
            'Records': [
                {
                    'eventSource': 'aws:s3',
                    'eventName': 'ObjectCreated:Put',
                    'eventTime': '2018-02-01T12:00:00Z',
                    's3': {
                        'bucket': {'name': 'test_bucket'},
                        'object': {'key': 'testfile.csv'},
                    },
                }
            ],
        })
        assert len(calls) == 1
        assert calls[0][:2] == ('test_bucket', 'testfile.csv')
        timestamp = calls[0][2]
        assert isinstance(timestamp, dt.datetime)
        assert str(timestamp) == '2018-02-01 12:00:00+00:00'

        # check that the message was processed
        assert queue.inflight == queue.visible == 0

    def test_multifile_message(self, queue):
        calls = self.check({
            # first message with 2 records
            'Records': [
                {
                    'eventTime': '2018-12-12',
                    's3': {
                        'bucket': {'name': 'bucket1'},
                        'object': {'key': 'path/to/file.txt'},
                    },
                },
                {
                    'eventTime': '2018-12-13',
                    's3': {
                        'bucket': {'name': 'bucket2'},
                        'object': {'key': 'another.file'},
                    },
                },
            ],
        }, {
            # second message with just 1 record
            'Records': [
                {
                    'eventTime': '2018-12-14',
                    's3': {
                        'bucket': {'name': 'bucket3'},
                        'object': {'key': 'yet/another/file'},
                    },
                },
            ],
        })

        # XXX we use "TZ-naive" timestamps here,
        # they shouldn't be expected in production usage.
        assert calls == [
            ('bucket1', 'path/to/file.txt', dt.datetime(2018, 12, 12)),
            ('bucket2', 'another.file', dt.datetime(2018, 12, 13)),
            ('bucket3', 'yet/another/file', dt.datetime(2018, 12, 14)),
        ]

        # test that all messages were consumed
        assert queue.inflight == 0
        assert queue.visible == 0

class TestCanProcess(BaseSkelTest):
    def test_basic(self, queue):
        tested = []
        processed = []

        class MyTestingFileProcessor(skel.BaseSQSFileProcessor):
            def process_file(self, bucket, key, timestamp):
                processed.append((bucket, key))

            def can_process_file(self, bucket, key):
                tested.append((bucket, key))
                return key.endswith('.yes')

        for key in [
            'dont/process.me',
            'do/process/me.yes',
            'another-file.unprocessed',
            'process-it.yes',
        ]:
            self.queue.send({
                'Records': [
                    {
                        'eventTime': '2018-12-10',
                        's3': {
                            'bucket': {'name': 'bucket1'},
                            'object': {'key': key},
                        },
                    },
                ],
            })
        MyTestingFileProcessor.main_standalone()

        assert tested == [
            ('bucket1', 'dont/process.me'),
            ('bucket1', 'do/process/me.yes'),
            ('bucket1', 'another-file.unprocessed'),
            ('bucket1', 'process-it.yes'),
        ]
        assert processed == [
            ('bucket1', 'do/process/me.yes'),
            ('bucket1', 'process-it.yes'),
        ]

        # all these messages should be removed
        # even ones we didn't actually process
        assert queue.visible == 0
        assert queue.inflight == 0

# TODO test other entry points, not just standalone one
# TODO check that failed messages are still available
