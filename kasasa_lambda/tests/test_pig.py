from glob import glob
import os.path as op
import sys
import tempfile
import warnings

from py.path import local as lp
import pytest

from .conftest import slow, importextra
get_pig, PigSession = importextra('pig', ('get_pig', 'PigSession'))
rmtree_if_exists = importextra('compat', 'rmtree_if_exists')
sh = pytest.importorskip('sh')


TEMPDIR = tempfile.gettempdir()

# autoused fixtures
@pytest.fixture(scope='session', autouse=True)
def _ensure_no_leftovers():
    rmtree_if_exists(PigSession.PIGDIR)


@pytest.fixture(scope='function', autouse=True)
def _avoid_leftovers_on_pigsession():
    # we want to make sure that PigSession class won't be changed by anything
    # FIXME is there a better way?
    backup = PigSession.__dict__.copy()
    yield
    for f in PigSession.__dict__:
        if f.startswith('__') and f.endswith('__'):
            continue
        if f in backup:
            setattr(PigSession, f, backup[f])
        else:
            delattr(PigSession, f)


# regular fixtures
@pytest.fixture
def disable_pigsession_warnings():
    # vvv actually this means "restore filters upon exit"
    with warnings.catch_warnings(record=False):
        warnings.filterwarnings(
            action='ignore',
            message='PigSession was not properly closed',
        )
        yield


@pytest.fixture
def pig(disable_pigsession_warnings):
    with get_pig() as pig:
        yield pig

    # clean up
    sh.rm('-rf', pig.PIGDIR)  # all flags etc are within it


# tests
def test_get_pig(disable_pigsession_warnings):
    # test that pig object is actually returned
    pig = get_pig()
    assert pig

    # test that pig is not a singleton
    pig2 = get_pig()
    assert pig2 is not pig

    assert pig.directory != pig2.directory
    assert pig.INDIR != pig2.INDIR
    assert pig.OUTDIR != pig2.OUTDIR


@slow
def test_ensure_ready(pig, mocker):
    mocker.spy(PigSession, '_prepare_environment')
    mocker.spy(PigSession, '_install_pig')

    # FIXME we probably don't want to actually download pig here, right?
    pig.ensure_ready()
    assert pig._prepare_environment.call_count == 1
    assert pig._install_pig.call_count == 1

    assert op.exists(pig.PIGDIR)
    assert op.exists(pig.INDIR)
    assert op.exists(pig.OUTDIR)

    # simulate we ran some script
    sh.mkdir(op.join(pig.OUTDIR, 'result.txt.dir'))
    sh.touch(op.join(pig.OUTDIR, 'result.txt.dir',  'part-123'))

    pig.ensure_ready()
    # make sure output directory still exists
    assert op.exists(pig.OUTDIR)
    # and check that install_pig was not called again
    assert pig._install_pig.call_count == 1


@pytest.mark.parametrize(
    'script', ['test', 'test.pig', '/test.pig'])
@slow
def test_run(pig, monkeypatch, mocker, script):
    # point it to the correct fake bucket location -
    # i.e. the tests directory where testing script resides
    monkeypatch.setattr(
        'kasasa_lambda.pig.PIG_SCRIPTS_BUCKET',
        op.relpath(op.dirname(__file__), op.curdir))

    pig.ensure_ready()

    # don't actually run any command
    def stub_pigcmd(args, **kw):
        # real pig script does create output directory,
        # so we want to replicate that behavior
        # but first let's parse params
        outputs = [
            p.split('=', 1)[1] for p in args
            if p.startswith('output')
        ]
        for output in outputs:
            sh.mkdir(output)
            # also create some sample output file
            sh.cat(_in='hello world\n', _out=op.join(output, 'part-1'))
            sh.cat(_in='abc def\n', _out=op.join(output, 'part-2'))
            # denote that we finished successfully
            sh.touch(op.join(output, '_SUCCESS'))

    pig.pigcmd = stub_pigcmd
    mocker.spy(pig, 'pigcmd')
    mocker.spy(pig, '_run_pig')

    # create input file
    # infile defaults to scriptname.txt
    infile = op.join(pig.INDIR, 'test.txt')
    with open(infile, 'w') as f:
        f.write('input world')
    # it will be removed automatically on pig uninitialization

    # run script
    pig.run(script)

    # We want to check that pigcmd was called with correct args,
    # but we don't want to check all the args - so assert_called_with won't work
    assert pig.pigcmd.call_count == 1
    # assert_called_once is not available in py2

    params, script = pig._run_pig.call_args[-2]

    assert params == {
        'input': op.join(pig.INDIR, 'test.txt'),
        'output': op.join(pig.OUTDIR, 'test.txt') + '.dir',
        'pigdir': pig.FULLPIGDIR,
    }
    assert script == op.join(pig.directory, 'test.pig')

    args, kwargs = pig.pigcmd.call_args[-2:]  # it is tuple of 2 or 3
    # but we want just last 2 values

    assert kwargs['_env']
    assert kwargs['_env'].get('JAVA_HOME')
    assert kwargs['_cwd'] == pig.directory
    assert kwargs['x'] == 'local'
    # make sure the full path was passed, regardless of what we gave
    assert kwargs['f'] == op.join(pig.directory, 'test.pig')

    # check that merged output file was created
    outfile = op.join(pig.OUTDIR, 'test.txt')
    assert op.exists(outfile)
    with open(outfile, 'r') as f:
        content = f.read()
    # content should be joined in the right order
    assert content == 'hello world\nabc def\n'

# TODO also somehow test simultaneous running?


class TestRunOnly(object):
    @pytest.fixture
    def pig(self, pig, mocker):
        """ "subclass" pig fixture for this class only. """
        # we don't want to unpack anything, hence stub _prep_env
        pig._prepare_environment = mocker.stub()
        # also we don't want to actually download script
        pig._download_script = mocker.stub()
        pig._run_pig = mocker.stub()
        pig._handle_output = mocker.stub()
        return pig

    @pytest.mark.parametrize(
        'script', ['myscript', 'myscript.pig',
                   op.join(TEMPDIR, 'scripts', 'myscript.pig')])
    def test_run_basic(self, pig, script):
        # we didn't initalize it yet, so it should not work
        with pytest.raises(ValueError, match='not ready'):
            pig.run(script)

        pig.ensure_ready()

        # input file still does not exist
        with pytest.raises(ValueError,
                           match=r'file does not exist.*myscript\.txt'):
            pig.run(script)

        # also check that absolute paths are not accepted for input
        with pytest.raises(ValueError, match=r'[Ii]nput.*should be relative'):
            pig.run(script, infile=op.join(TEMPDIR, 'somefile.txt'))

        # create sample file
        lp(pig.INDIR).join('myscript.txt').write('hello world')

        # it should not be called yet because of errors
        assert pig._run_pig.call_count == 0

        # now it should work
        pig.run(script)

        pig._run_pig.assert_called_with(
            dict(
                pigdir=pig.FULLPIGDIR,
                input=op.join(pig.INDIR, 'myscript.txt'),
                output=op.join(pig.OUTDIR, 'myscript.txt.dir'),
            ),
            op.join(pig.directory, 'myscript.pig'),
        )
        pig._handle_output.assert_called_with(
            'output',
            op.join(pig.OUTDIR, 'myscript.txt.dir'),
            op.join(pig.OUTDIR, 'myscript.txt'),
        )

        # check that output files are required to not include any path
        with pytest.raises(ValueError,
                           match=r'[Oo]utput.*should not include path'):
            pig.run(script, outfile='path/to/output.txt')

    @pytest.mark.parametrize(
        'infiles', [
            ['phones.txt', 'names.txt'],
            {'phones': 'phones.txt', 'names': 'names.txt'},
        ])
    def test_run_multiple_inputs(self, pig, infiles):
        pig.ensure_ready()

        script = 'whatever.pig'

        with pytest.raises(ValueError):
            # test that input files are checked for existence
            pig.run(script, infile=infiles)

        # create dummy input files
        for f in (infiles.values() if hasattr(infiles, 'values') else infiles):
            lp(pig.INDIR).join(f).write('data: ' + f)

        pig.run(script, infile=infiles)

        assert pig._run_pig.call_count == 1
        args = pig._run_pig.call_args[-2]
        params, script = args

        assert script == op.join(pig.directory, script)

        inparams = {k: v for k, v in params.items() if k.startswith('input')}
        if isinstance(infiles, list):
            assert inparams == {
                'input1': op.join(pig.INDIR, 'phones.txt'),
                'input2': op.join(pig.INDIR, 'names.txt'),
            }
        elif isinstance(infiles, dict):
            assert inparams == {
                'input_phones': op.join(pig.INDIR, 'phones.txt'),
                'input_names': op.join(pig.INDIR, 'names.txt'),
            }
        else:
            raise TypeError(infiles)

    def test_run_multiple_outputs(self, pig):
        pig.ensure_ready()

        script = 'whatever.pig'

        # create dummy input file
        lp(pig.INDIR).join('whatever.txt').write('data')

        pig.run(script, outfile={
            'cars': 'some_cars.txt',
            'roads': 'all-roads.txt',
        })

        assert pig._run_pig.call_count == 1
        args = pig._run_pig.call_args[-2]
        params, script = args
        outparams = {k: v for k, v in params.items() if k.startswith('output')}
        assert outparams == {
            'output_cars': op.join(pig.OUTDIR, 'some_cars.txt.dir'),
            'output_roads': op.join(pig.OUTDIR, 'all-roads.txt.dir'),
        }
        assert pig._handle_output.call_count == 2
        pig._handle_output.assert_any_call(
            'output_cars',
            op.join(pig.OUTDIR, 'some_cars.txt.dir'),
            op.join(pig.OUTDIR, 'some_cars.txt'),
        )
        pig._handle_output.assert_any_call(
            'output_roads',
            op.join(pig.OUTDIR, 'all-roads.txt.dir'),
            op.join(pig.OUTDIR, 'all-roads.txt'),
        )

        pig.run(script, outfile=['people.txt', 'animals.txt'])

        params = pig._run_pig.call_args[-2][0]
        outparams = {k: v for k, v in params.items() if k.startswith('output')}

        assert outparams == {
            'output1': op.join(pig.OUTDIR, 'people.txt.dir'),
            'output2': op.join(pig.OUTDIR, 'animals.txt.dir'),
        }
        pig._handle_output.assert_any_call(
            'output1',
            op.join(pig.OUTDIR, 'people.txt.dir'),
            op.join(pig.OUTDIR, 'people.txt'),
        )
        pig._handle_output.assert_any_call(
            'output2',
            op.join(pig.OUTDIR, 'animals.txt.dir'),
            op.join(pig.OUTDIR, 'animals.txt'),
        )


class TestHandleOutput(object):
    nbsp = u'\xA0'
    if sys.version_info[0] < 3:
        # convert it to bytestring
        # to work correctly with non-unicode templates
        nbsp = nbsp.encode('utf-8')

    @pytest.fixture
    def pig(self, pig, mocker):
        # we don't want to unpack anything, hence stub _prep_env
        pig._prepare_environment = mocker.stub()
        return pig

    @pytest.fixture(autouse=True)
    def _workdata(self, tmpdir):
        self.pname = 'output_xyz'
        self.output = tmpdir.join('something.txt.dir')
        self.target = tmpdir.join('something.txt')

    @pytest.fixture
    def ho(self, pig):
        # just a shorthand
        return (lambda name=self.pname,
                output=self.output.strpath,
                target=self.target.strpath:
                pig._handle_output(name, output, target))

    def test_corner_cases(self, ho):
        def assert_raises(match):
            with pytest.raises(ValueError, match=match):
                ho()

        # check that we require existing odir
        assert_raises('target directory .* was not created by pig')

        self.output.mkdir()
        # check that we'll fail if we cannot recognize directory contents
        assert_raises('neither single nor subdir')

        # simulate this is a usual PigStorage output
        self.output.join('part-1234').write('something')
        # check that we require _SUCCESS flag
        assert_raises('exists but has no _SUCCESS flag')

        self.output.join('_SUCCESS').write('')

        # check that we require non-existing target dir
        # so file won't work
        self.target.write('this is a file!')
        assert_raises('already exists')

        # and dir won't work as well
        self.target.remove()
        self.target.mkdir()
        assert_raises('already exists')

    def test_single_output(self, ho):
        o = self.output
        t = self.target

        o.yaml_create(
            r'''
            _SUCCESS: ''
            part-0000: "hello\nworld"
            part-1234: "test\ndata"
        ''')

        ho()

        # check that it is just a single file with given data
        assert t.yaml_check(r'''
            "hello\nworld\ntest\ndata\n"
        ''')

    def test_multi_storage(self, ho):
        o = self.output
        t = self.target

        o.yaml_create(r'''
            _SUCCESS: ''
            ghost:
                ghost-0{nbsp}000: "aww\noww"
                ghost-0{nbsp}001: "aahh\nooohh"
            drill:
                drill-0{nbsp}000: "drrrr\ntrrr"
        '''.format(nbsp=self.nbsp))

        ho()

        t.yaml_check(r'''
            ghost.txt: "aww\noww\naahh\nooohh\n"
            drill.txt: "drrrr\ntrrr\n"
        ''')

    def test_multiple_dirs(self, ho):
        o = self.output
        t = self.target

        # simulate these constructs:
        # '$output/names' PigStorage
        # '$output/addresses' MultiStorage
        # '$output/multipart/computers' PigStorage
        # '$output/multipart/networks' MultiStorage
        # latter ones require correct handling of .txt suffix
        o.yaml_create(r'''
            names:
                _SUCCESS: ''
                part-0000: names data
            addresses:
                _SUCCESS: ''
                cities:
                    cities-0{nbsp}000: cities data
                streets:
                    streets-0{nbsp}000: streets data
            multipart:
                computers:
                    _SUCCESS: ''
                    part-0000: some computers
                networks:
                    _SUCCESS: ''
                    local:
                        local-0{nbsp}000: local net data
                    remote:
                        remote-0{nbsp}000: remote net data
        '''.format(nbsp=self.nbsp))

        ho()

        t.yaml_check(r'''
            names.txt: "names data\n"
            addresses:
                cities.txt: "cities data\n"
                streets.txt: "streets data\n"
            multipart:
                computers.txt: "some computers\n"
                networks:
                    local.txt: "local net data\n"
                    remote.txt: "remote net data\n"
        ''')


def test_corner_cases(pig, mocker):
    pig._prepare_environment = mocker.stub()

    # run on uninitialized pig
    with pytest.raises(ValueError, match='PigSession is not ready'):
        pig.run('anything')

    # filename validation - should not include path or at least be relative
    pig.ensure_ready()
    with pytest.raises(ValueError):
        pig.run('anyscript', op.join(TEMPDIR, 'desired-output.txt'))
    with pytest.raises(ValueError):
        pig.run('anyscript', './desired-output.txt')


def test_rotate_directories(pig):
    # for simplicity we don't want to fully initialize this pig,
    # just make sure it creates any directories required
    pig._make_directories()
    pig.ready = True

    root = lp(pig.OUTDIR)

    # let's prepare some directories and files
    for path in [
        'phones/124',
        'phones/125',
        'phones/128',
        'names/121',
        'names/125',
        'names/126',
    ]:
        d, f = path.split('/')
        fdir = root.join(d)
        if not fdir.exists():
            fdir.mkdir()
        # write file's name as its content
        fdir.join(f + '.txt').write(path)

    # do the job
    pig.rotate_directories('phones', 'names')

    # now test that result is good
    for d in 'phones', 'names':
        assert not root.join(d).exists()

    outfiles = glob(root.join('*/*').strpath)
    assert sorted(
        # just leave last dir and filename
        '/'.join(f.rsplit('/', 2)[-2:])
        for f in outfiles
    ) == [
        '121/names.txt',
        '124/phones.txt',
        '125/names.txt',
        '125/phones.txt',
        '126/names.txt',
        '128/phones.txt',
    ]
    for f in map(lp, outfiles):
        orig_dir = f.purebasename
        orig_fname = f.dirpath().basename
        assert f.read() == orig_dir + '/' + orig_fname

    # now test corner case handling:
    # missing directories,
    with pytest.raises(ValueError, match='not exists'):
        pig.rotate_directories('missing')
    # file instead of directory,
    root.join('itsafile').write('hello!')
    with pytest.raises(ValueError, match='not a directory'):
        pig.rotate_directories('itsafile')
    # and nested subdirectories
    root.join('dir', 'subdir').ensure(dir=True)
    with pytest.raises(ValueError, match='not a regular file'):
        pig.rotate_directories('dir')
