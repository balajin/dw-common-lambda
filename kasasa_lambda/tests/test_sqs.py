import boto3
from botocore.exceptions import ClientError
import moto
import pytest

from kasasa_lambda.sqs import get_sqs, SQS, Message


@pytest.fixture(autouse=True)
def moto_sqs(moto_credentials):
    with moto.mock_sqs():
        yield boto3.resource('sqs')


@pytest.fixture
def queue(moto_sqs):
    q = moto_sqs.create_queue(QueueName='test')
    return q


@pytest.fixture
def sqs(queue):
    return get_sqs(queue.url)


def test_sqs_creation(moto_sqs):
    with pytest.raises(Exception):
        # name or url shall be passed
        get_sqs()

    # make queue for futher testing against it
    q = moto_sqs.create_queue(QueueName='test1')

    # try with url
    sqs = get_sqs(q.url)
    # and with name
    sqs = get_sqs('test1')

    # wrong names and urls should not work
    # FIXME this is temporarily disabled until fix of
    # https://github.com/spulec/moto/issues/1251
    #with pytest.raises(ClientError, match='queue does not exist'):
    with pytest.raises(Exception):
        get_sqs('http://no-such-queue')
    # FIXME
    #with pytest.raises(ClientError, match='queue does not exist'):
    with pytest.raises(Exception):
        get_sqs(q.url + 'and-something-wrong')
    with pytest.raises(ClientError, match='queue does not exist'):
        get_sqs('unknown-queue-name')
    #with pytest.raises(ClientError, match='queue does not exist'):
    with pytest.raises(Exception):
        get_sqs('incorrect://schema')


@pytest.mark.xfail(reason='Sending not implemented yet')
def test_sqs_sending(queue, sqs):
    sqs.send()  # TODO


def test_sqs_receiving(queue, sqs):
    ret = sqs.receive()
    assert ret == []

    # and with no waiting
    assert sqs.receive(wait=None) == []

    # pre-fill queue with soe messages
    queue.send_message(MessageBody='hello')
    queue.send_message(MessageBody='{"answer": 42}')
    queue.send_message(MessageBody='{"invalid_json')
    for i in range(7):
        # fill to get 10 messages
        queue.send_message(MessageBody='msg%s' % i)

    ret = sqs.receive(wait=0)
    assert len(ret) == 1
    msg = ret[0]
    # it is not a json, so it should return just the string
    assert msg.data == 'hello'

    # this one is valid json and should be decoded properly
    msg, = sqs.receive(wait=0)
    assert msg.data == {'answer': 42}

    msg, = sqs.receive(wait=0)
    assert msg.data == '{"invalid_json'

    ret = sqs.receive(max_count=10, wait=0)
    assert len(ret) == 7

    # Note that we didn't delete any message, so they all would resurrect
    # after some delay.
    # Next test is for message deletion.


def test_sqs_deletion(queue, sqs):
    def assert_meta(name, val):
        # make sure data is fresh
        queue.load()
        print(queue.attributes)
        assert int(queue.attributes[name]) == val

    def assert_stats(visible, hidden):
        queue.load()
        assert int(queue.attributes['ApproximateNumberOfMessages']) == visible
        assert int(
            queue.attributes['ApproximateNumberOfMessagesNotVisible']
        ) == hidden

    queue.send_message(MessageBody='hello')
    queue.send_message(MessageBody='world')
    queue.send_message(MessageBody='abc')

    assert_stats(3, 0)

    msg, = sqs.receive(wait=0)

    assert_stats(2, 1)

    msg.delete()

    assert_stats(2, 0)

    # use different method - remove message by its handle
    msg, = sqs.receive(wait=0)
    sqs.delete(msg.handle)

    assert_stats(1, 0)

    # and the third way is to use with statement

    msg, = sqs.receive(wait=0)
    # it should not be deleted on error...
    with pytest.raises(Exception, message='test'):
        with msg:
            raise Exception('test')
    assert_stats(0, 1)  # it is still invisible because timeout is not exceeded

    with msg:
        assert_stats(0, 1)
    # now it is deleted
    assert_stats(0, 0)
