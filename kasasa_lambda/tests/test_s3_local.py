import os
import os.path as op
import shutil

import pytest

from .conftest import importextra
LocalS3 = importextra('s3', 'LocalS3')


def test_local_s3_creation():
    with pytest.raises(Exception) as exc:
        LocalS3('invalid-bucket-name')
    assert 'No such local bucket' in str(exc.value)

    # make sure it won't raise anything
    ls3()

    # TODO also test virtual bucket location algorithm

@pytest.fixture
def ls3():
    # we will use `tests` dir itself as a testing bucket
    # but we cannot predict where is the current directory
    # (relative to which the bucket is located),
    # so let's calculate relpath
    return LocalS3(op.relpath(
        op.dirname(__file__),
        op.curdir,
    ))

def test_local_s3_list(ls3):
    keys = list(ls3.list())
    assert 'test_s3_local.py' in keys
    assert '__init__.py' in keys
    assert '..' not in keys
    assert '.' not in keys

def test_local_s3_iterable(ls3):
    keys = list(ls3)
    assert '__init__.py' in keys

def test_local_s3_download(ls3, tmpdir):
    # technically, py3's tmpdir is localpath, not str
    # and it doesn't support endswith -
    # so let's convert it
    tmpdir = str(tmpdir)

    KEY = 'test_s3_local.py'
    OUT = op.join(tmpdir, KEY)

    ls3.download(KEY, tmpdir + op.sep)
    assert op.exists(OUT)
    assert op.getsize(OUT)

    OUT = op.join(tmpdir, 'another.file.name')
    ls3.download(KEY, OUT)
    assert op.exists(OUT)
    assert op.getsize(OUT)

    with pytest.raises(ValueError):
        ls3.download('no.such.file', OUT)

def test_local_s3_upload(ls3, tmpdir):
    src = tmpdir.join('anyfile.typ')
    content = os.urandom(100)
    src.write_binary(content)

    fulldst = op.join(op.dirname(__file__), op.basename(src.strpath))

    try:
        ls3.upload(src.strpath)
        assert op.exists(fulldst)
        with open(fulldst, 'rb') as dst:
            assert dst.read() == content
    finally:
        os.remove(fulldst)

    # and with explicit dest name
    dst = 'hello_world.txt'
    fulldst = op.join(op.dirname(__file__), dst)
    try:
        ls3.upload(src.strpath, dst)
        assert op.exists(fulldst)
        with open(fulldst, 'rb') as dst:
            assert dst.read() == content
    finally:
        os.remove(fulldst)

    # also should support uploading to subdirs
    dst = 'path/to/hello_world.bin'
    fulldst = op.join(op.dirname(__file__), dst)
    try:
        ls3.upload(src.strpath, dst)
        assert op.exists(fulldst)
        with open(fulldst, 'rb') as dst:
            assert dst.read() == content
    finally:
        shutil.rmtree(op.join(op.dirname(__file__), 'path'))

    # check uploading directories
    src = tmpdir.join('dir_to_upload')
    src.join('subfile').write('hello world', ensure=True)
    src.join('subdir', 'file.txt').write('hello', ensure=True)
    dst = 'whatever/'
    fulldst = op.join(op.dirname(__file__), dst)
    try:
        ls3.upload(src.strpath, dst)
        assert op.exists(fulldst)
        assert op.isdir(fulldst)
        udir = op.join(fulldst, 'dir_to_upload')
        assert op.isdir(udir)
        with open(op.join(udir, 'subfile'), 'r') as f:
            assert f.read() == 'hello world'
        with open(op.join(udir, 'subdir', 'file.txt'), 'r') as f:
            assert f.read() == 'hello'

        # check overwriting existing objects
        src.join('subdir', 'file.txt').write('another data')
        with pytest.raises(ValueError):
            ls3.upload(src.strpath, dst)  # overwrite not enabled
        ls3.upload(src.strpath, dst, overwrite=True)
        with open(op.join(udir, 'subdir', 'file.txt'), 'r') as f:
            assert f.read() == 'another data'
    finally:
        shutil.rmtree(fulldst)
