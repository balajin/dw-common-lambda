import sys
import os
import os.path as op

from dotenv import find_dotenv, load_dotenv

# we want to use backported csv module for py2
# but built-in for py3.
# don't use try..except because we don't want to use py2's built-in module.
if sys.version_info < (3, 0):
    from backports import csv
else:
    import csv

# load .env file if it exists
denv = find_dotenv()
if denv:
    load_dotenv(denv)
# boto3 uses complex algorithm to find credentials;
# the first place it looks for them if not provided from the code
# is environment variables.
# So if we create file `.env` with variables
# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY (also AWS_DEFAULT_REGION),
# they will be used instead of machine-global values.
# This is useful to specify credentials on per-project basis.


def touch(fname):
    """
    Create file with given nae unless already exists.
    Notice that unlike "real" touch program this doesn't update timestamp
    on existing files. (right?)
    """
    with open(fname, 'a'):
        pass


def rotate_directories(root, *dirs):
    """
    This is an implementation of pig.rotate_directories.
    It is separated to avoid coupling the logic with pig class -
    from pig class we need only OUTDIR as root.
    """
    # read information about original data,
    # and validate data
    files = []
    for d in dirs:
        if not op.isdir(op.join(root, d)):
            raise ValueError('{} is not a directory or not exists'.format(
                d))
        for f in os.listdir(op.join(root, d)):
            if not op.isfile(op.join(root, d, f)):
                raise ValueError('{} is not a regular file, aborting'
                                 .format(op.join(d, f)))
            name, ext = op.splitext(f)
            files.append(
                (d, name, ext)
            )

    for d, f, ext in files:
        src = op.join(root, d, f+ext)
        dst = op.join(root, f, d+ext)

        # renames work excellently for us:
        # it creates new directories as necessary
        # and removes any left empty.
        os.renames(src, dst)


class DoubleDelimiterCsvBase(object):
    def __init__(self, delimiter, **kw):
        # make sure delimiter is str/unicode;
        # this is required by backported csv module,
        # as well as by py3's csv.
        # `bytes` works for both py2 and py3.
        if isinstance(delimiter, bytes):
            delimiter = delimiter.decode()

        if len(delimiter) == 1:
            self.double = False
            self.delimiter_char = delimiter
        elif len(delimiter) == 2 and delimiter[0] == delimiter[1]:
            self.double = True
            self.delimiter_char = delimiter[0]  # for underlying reader/writer
        else:
            raise ValueError('Unsupported delimiter %r' % delimiter)

        if self.double:
            dialect = kw.get('dialect', csv.excel)
            quoting = kw.get('quoting', dialect.quoting)
            if quoting == csv.QUOTE_ALL:
                raise ValueError(
                    'We cannot write double delimiters when QUOTE_ALL is used',
                    # that is because our faux empty fields
                    # would be quoted as well, breaking the whole idea
                )


class DoubleDelimiterCsvReader(DoubleDelimiterCsvBase):
    def __init__(self, f, delimiter, **kw):
        super(DoubleDelimiterCsvReader, self).__init__(delimiter, **kw)

        self.reader = csv.reader(f, delimiter=self.delimiter_char, **kw)

    def __iter__(self):
        return self

    def __next__(self):
        row = next(self.reader)
        if not self.double:
            return row
        # make sure that format was correct:
        # odd number of fields
        assert len(row) % 2 == 1
        # and all even fields are empty
        assert all(not f for f in row[1::2])
        # then just return all odd fields
        return row[0::2]
    next = __next__  # support both py2 and py3

    @property
    def line_num(self):
        # proxy
        return self.reader.line_num


class DoubleDelimiterCsvWriter(DoubleDelimiterCsvBase):
    """
    Wrap csv.writer to support double-char delimiters.
    This class supports both standard (single-char)
    and double-identical-chars delimiters, like `||`.

    This class works by instructing `csv.writer` to use single-char delimiters
    and inserting "fake" empty fields to form double delimiters.
    As a result, it does not support non-identical-char delimiters
    and does not allow QUOTE_ALL which would quote our empty fields.
    """
    def __init__(self, f, delimiter, **kw):
        super(DoubleDelimiterCsvWriter, self).__init__(delimiter, **kw)

        self.writer = csv.writer(f, delimiter=self.delimiter_char, **kw)

    def _convert(self, row):
        if self.double:
            row = sum(
                # for each record, append an empty one
                ([item, ''] for item in row),
                []
            )[:-1]  # and remove the last empty record
        return row

    def writerow(self, row):
        return self.writer.writerow(self._convert(row))

    def writerows(self, rows):
        return self.writer.writerows(map(self._convert, rows))


class DDCsvDictReader(csv.DictReader):
    def __init__(self, f, fieldnames=None, restkey=None, restval=None,
                 delimiter=None, **kw):
        # it will create regular csv.reader...
        csv.DictReader.__init__(self, f, fieldnames, restkey, restval)
        # and we will overwrite it with our fancy reader
        self.reader = DoubleDelimiterCsvReader(f, delimiter, **kw)
        # the rest should work as expected


class DDCsvDictWriter(csv.DictWriter):
    def __init__(self, f, fieldnames, restval='', extrasaction='raise',
                 delimiter=None, **kw):
        # same logic as with reader
        csv.DictWriter.__init__(self, f, fieldnames, restval, extrasaction)
        self.writer = DoubleDelimiterCsvWriter(f, delimiter, **kw)
