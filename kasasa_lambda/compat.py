import os.path as op
import os
import shutil
try:
    # py3
    import urllib.parse as urlparse  # noqa
except ImportError:
    # py2
    import urlparse  # noqa

try:
    from tempfile import TemporaryDirectory  # noqa
except ImportError:
    from backports.tempfile import TemporaryDirectory  # noqa


def rmtree_if_exists(path):
    if op.exists(path):
        if op.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


# python2 doesn't support exist_ok flag for makedirs, hence -
def makedirs_unless_exist(path):
    if op.isdir(path):
        return
    return os.makedirs(path)
