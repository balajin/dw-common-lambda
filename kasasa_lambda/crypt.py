"""
This module wraps GnuPG_ module
to provide less rich but simpler interface.
It also adds a bit of integrtion with S3 (`import_keys_from_s3`).

.. _GnuPG: https://github.com/isislovecruft/python-gnupg
"""

import logging
import shutil
import tempfile

import gnupg

from .s3 import get_s3


logger = logging.getLogger(__name__)


class GpgHelper(object):
    """
    Wrapper around ``GnuPG`` to simplify en/decrypting files.

    Usage:

    * Obtain new `GpgHelper` instance using :func:`get_gpg`
      and pass it a password for your private key.
    * Import keys (:meth:`import_keys`), optionally from S3 bucket
      (:meth:`import_keys_from_s3`).
    * Encrypt or decrypt what you need to
      (:meth:`encrypt_file`, :meth:`decrypt_file`).
    * Remove artifacts (:meth:`cleanup`).
      The helper by default will store GPG data in a temporary directory
      unless you use custom ``homedir``.

    This class can be used as a context manager
    in which case it will clean up itself after usage::

        with get_gpg(password) as gpg:
            gpg.import_keys(private_blob, public_blob)
            gpg.encrypt_file(src, dst)
        # At this point all temporary artifacts are cleaned up
    """
    def __init__(self, password, sign=True, check_signature=True,
                 homedir=None):
        """
        :param str password: password for private key
        :param bool sign: whether to sign encrypted files by default
            (can be overriden when calling :meth:`encrypt_file`)
        :param bool check_signature: whether to check signature
            when decrypting file
            (can be overriden when calling :meth:`decrypt_file`)
        :param str homedir: store keys in given directory instead of home dir,
            and don't remove that directory on `cleanup`.
            If you use this option
            then you might not want to call :meth:`import_keys`.

        .. todo:: add a method to determine which keys do we already have,
           load their hashes to ``self.{private,public}_keyhash``
           and determine which of them is ours and which is theirs;
           this would make it unnecessary
           to call :meth:`import_keys` every time.

        Attributes:
            home (str): path to GPG home directory
            rmhome (bool): whether we want to remove our home directory
                on cleanup
            gpg: GnuPG object we use
            password (str): private key's password
            sign (bool): whether we want to sign encrypted files by default
            check_signature (bool):
                whether we want to check signature by default
            known_keys: hashes of keys we know, used when checking signature
            private_keyhash:
            public_keyhash:
                hashes of the private/public keys we will use
        """
        # FIXME use fixed directory, not tempdir,
        # and persist key across runs when possible
        if homedir:
            self.home = homedir
            self.rmhome = False
        else:
            self.home = tempfile.mkdtemp()
            self.rmhome = True
        self.gpg = gnupg.GPG(gnupghome=self.home)
        self.password = password
        self.sign = sign
        self.check_signature = check_signature
        self.known_keys = set()
        self.private_keyhash = self.public_keyhash = None

    def _import_key(self, data):
        """
        Import key(s) from given data which is an armored encoding of the key.
        Will return hash for the first key imported from that data string.
        """
        ir = self.gpg.import_keys(data)
        if not ir:
            logger.error('Failed to import key:')
            logger.error(ir.stderr)
            raise ValueError('Failed to import key')
        self.known_keys.update(ir.fingerprints)
        return ir.fingerprints[0]

    def import_keys(self, private, public):
        """
        Load keys from two different blobs and import them.

        Will assign `.private_keyhash` and `.public_keyhash`
        with hashes of first keys of corresponding strings.

        :param str private: string containing armored encoding
            of our private key
        :param str public: string containing armored encoding
            of partner's public key

        .. note::

           We want to distinguish
           between "our" (private) and "their" (public) keys.
           One way would be to load them from single file
           and then just be told their hashes.
           Another way is to load them from different files
           and automatically determine hashes.
           For now we will use that second way.
        """
        logger.info('Importing private key...')
        self.private_keyhash = self._import_key(private)
        logger.info('Importing public key...')
        self.public_keyhash = self._import_key(public)
        # we want also "additional" fingerprints if any -
        # they are used when checking signature
        self.known_keys.update(
            k['fingerprint']
            for k in self.gpg.list_keys()
        )
        logger.info('Done importing keys')

    def import_keys_from_s3(self, bucket, private, public):
        """
        Like :meth:`.import_keys` but loads armored values from ``s3`` bucket.

        :param str bucket: name of S3 bucket to load data from.
        :param str private: S3 key name of the object
            containing private (our) key.
        :param str public: S3 key name of the object
            containing public (their) key.
        """
        s3 = get_s3(bucket)
        self.import_keys(
            s3.fetch(private),
            s3.fetch(public),
        )

    def encrypt_file(self, src, dst, sign=None, armor=False):
        """
        Encrypt given file ``src`` with public key
        and write result to ``dst`` file.
        Will optionally sign it with our private key.

        :param bool sign: whether to also sign file with private key
            in addition to encrypting it with public key;
            by default will refer to instance-global value `.sign`.
        :param bool armor: whether to produce ASCII-armored output
            (default is binary)
        """
        if sign is None:
            sign = self.sign  # use default

        logger.info('Will encrypt file %s to %s %s signing and %s armoring',
                    src, dst, 'with' if sign else 'without',
                    'with' if armor else 'without')

        # XXX will it properly use private key for signing?
        with open(src, 'rb') as f:
            ret = self.gpg.encrypt_file(
                f, self.public_keyhash,
                always_trust=True,
                passphrase=self.password,
                sign=sign,
                armor=armor,
                output=dst,
            )
        if not ret.ok:
            logger.error('Failed to encrypt:')
            logger.error(ret.stderr)
            raise ValueError('Encryption failed')

        logger.info('Done encrypting')

    def decrypt_file(self, src, dst, check_signature=None):
        """
        Decrypt given file from ``src`` into ``dst`` path.
        Will optionally check signature.

        :param bool check_signature: whether to check
            encrypted file's signature
            and fail if it doesn't match.
            Defaults to instance-global value `.check_signature`.
        """
        if check_signature is None:
            check_signature = self.check_signature  # use default

        logger.info('Will decrypt file %s to %s %s checking signature',
                    src, dst, 'with' if check_signature else 'without')

        with open(src, 'rb') as f:
            ret = self.gpg.decrypt_file(
                f,
                always_trust=True,
                passphrase=self.password,
                # don't use output arg because we want to check signature
            )
        if not ret.ok:
            logger.error('Failed to decrypt:')
            logger.error(ret.stderr)
            raise ValueError('Decryption failed')

        if check_signature:
            if not ret.signature_id:
                logger.error('File is not signed')
                raise ValueError('File is not signed')
            if ret.fingerprint not in self.known_keys:
                raise ValueError(
                    'Signature fingerprint {} is not known;'
                    'known keys: {}'.format(
                        ret.fingerprint,
                        ', '.join(self.known_keys),
                    ),
                )

        with open(dst, 'wb') as f:
            logger.debug('Writing resulting data...')
            f.write(ret.data)

        logger.info('Decryption done')

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.cleanup()

    def __del__(self):
        if self.home:
            self.cleanup()

    def cleanup(self):
        """
        Remove any temporary artifacts,
        namely temp directory which was used for GPG data.

        Will also be called when exiting context manager block.
        Will also be called when destroying the class (``__del__``)
        """
        if self.rmhome:
            assert self.home, 'Already cleaned up?'
            shutil.rmtree(self.home)
        self.home = None


def get_gpg(*args, **kwargs):
    """
    Convenience wrapper for `GpgHelper` constructor.
    """
    return GpgHelper(*args, **kwargs)
