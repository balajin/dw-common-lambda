# don't consider unused imports
# pylama: ignore=W0611

import logging
import os

# thise should always be supported as they use only boto3
from . import misc  # import it explicitly for dotfile support
from .kms import get_kms, kms_decrypt
from .s3 import get_s3, get_secrets
from .sqs import get_sqs
from .sns import get_sns
from .alambda import get_lambda
from .sts import get_sts, get_sts_session


__version__ = '1.15.0'


if not os.environ.get('NO_FORCE_LOGGING'):
    # Automatically enable logging to console,
    # just in case we forgot to do it in the handler
    logging.basicConfig(level=logging.INFO)
    # it won't work in lambda where logging is already configured,
    # so separately for lambdas set loglevel for root logger
    logging.getLogger().setLevel(logging.INFO)


class __not_installed(object):
    """
    Certain feature's dependencies were not installed.
    Let's raise a descriptive exception.
    """
    def __init__(self, name):
        self.name = name
        self.__name__ = name

    def __call__(self, *args, **kwargs):
        raise ValueError(
            'Extra "{e}" is not installed! Please install it like this: '
            'pip install kasasa_lambda[{e}]'.format(e=self.name)
        )

    def __getattr__(self, key):
        return self()  # raise

    def __repr__(self):
        return '<__not_installed: {}>'.format(self.name)


try:
    from .db import get_mysql_connection, parse_db_envvars, parse_db_url
    from .db import get_db_connection  # deprecated
except ImportError:
    get_db_connection = __not_installed('db')
    pass

try:
    from .pig import get_pig
except ImportError:
    get_pig = __not_installed('pig')

try:
    from .crypt import get_gpg
except ImportError:
    get_gpg = __not_installed('crypt')

try:
    from . import xray  # noqa
except ImportError:
    xray = __not_installed('xray')
