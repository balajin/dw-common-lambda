"""
This module contains skeleton implementations of several useful algorithms
which can be used as a base for lambda functions and other components.
"""

import logging
import os

from dateutil.parser import parse as dateutil_parse
from kasasa_lambda import get_sqs, get_lambda, xray_optional
from kasasa_lambda.sqs import Message as SQSMessage


logger = logging.getLogger(__name__)


class collect_stats(object):
    """
    This class can be used as a decorator
    to wrap a method with stats storing gear.
    Technically it is a non-data descriptor
    which is similar to py2's "unbound method" descriptor
    but wraps function with a special object
    to collect stats about successful and failed launches.

    Unlike "bound method" object (which is created again on each access),
    we need to cache our StatsCollectorBoundMethod object,
    or else stats will be reset constantly.
    """
    def __init__(self, fn):
        self._fn = fn
        # In py2 we cannot know the name under which we are stored.
        # XXX hack: use function's name and hope it is the same
        # (which is the case when decorator is used)
        self._name = fn.__name__
        # TODO maybe better use string based on id(self)
        # for guaranteed uniqueness...
        # but it is not required in our usecase.

    def __set_name__(self, owner, name):
        # In py3, we can know which name we were assigned to.
        # So use that name for caching instead of the random one.
        # That way, we override ourself on instance level when first requested,
        # and subsequent requests go to the cached version (speed up!)
        self._name = name

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        # getattr would use __get__ so access __dict__ directly
        collector = instance.__dict__.get(self._name)
        if collector is None:
            collector = StatsCollectorBoundMethod(self._fn, instance)
            setattr(instance, self._name, collector)
        return collector


class StatsCollectorBoundMethod(object):
    def __init__(self, fn, obj):
        self._fn = fn
        self._obj = obj

        self.total = 0
        self.success = 0
        self.fail = 0
        self.last_exc = None

    def __call__(self, *args, **kwargs):
        self.total += 1
        try:
            self.success += 1
            return self._fn(self._obj, *args, **kwargs)
        except Exception as e:
            self.success -= 1
            self.fail += 1
            self.last_exc = e
            raise


class BaseSQSFileProcessor(object):
    """
    Implements base algorithms for processing
    S3 objects based on SQS notifications.

    Usage: subclass it in your module and override some methods;
    then call desired entry points - either directly or from a function.

    Example::

        from kasasa_lambda.skel import BaseSQSFileProcessor


        class MyHandler(BaseSQSFileProcessor):
            def load_env(self):
                # important: don't forget to call super!
                super(MyHandler, self).load_env()

                # here you can load environment
                # and do other cheap preparations

            def can_process_file(self, bucket, key):
                # return True to process file (which is default)
                # or False to skip it.

            def process_file(self, bucket, key, timestamp):
                # Do whatever you want with the file.
                # For example, copy it somewhere.

    Now if you want to use it in a worker, call corresponding endpoint::

        if __name__ == '__main__':
            MyHandler.main_standalone()

    Or if you want to use it in lambda, expose desired entry point::

        def handle_one_by_one(event, context):
            return MyHandler.lambda_main_one_by_one(event, context)

    Or even simpler::

        handle_one_by_one = MyHandler.lambda_main_one_by_one  # no parentheses!

    (Lambda does not currently allow us
    to specify class method directly as an endpoint,
    hence we have to do such trick.)
    """

    def load_env(self):
        """
        Load all required environment variables.
        This is implemented as a separate method which is called early,
        so that any missing variables will be detected early.

        Override this method if you want to load any additional settings
        and don't forget to call ``super(YourClass, self).load_env()``.

        It is important to not make any database connections here,
        because this method might be called even when we don't plan
        to do any real processing,
        e.g. when there are no messages
        or when current lambda is going to pass messages to another lambda.
        Hence it should be cheap but still do basic validation,
        like check if certain environment variable exists
        or if its value looks like a correct one.

        Default implementation recognizes the following environment variables:

        .. envvar:: QUEUE_URL

           URL of the SNS queue to receive messages from.

        .. envvar:: STOP_ON_ERRORS

           For main methods supporting that,
           settings this variable to nonempty value will request
           to stop handling after any message processing failed.
           Useful for debugging.

        .. envvar:: MESSAGES_COUNT

           For batch-handling main methods
           (like :meth:`lambda_main_one_by_one` and :meth:`lambda_main_gevent`)
           specifies how many messages should be processed in one batch.
           Defaults to ``10``.
        """
        self.queue_url = os.environ['QUEUE_URL']
        self.stop_on_errors = bool(os.environ.get('STOP_ON_ERRORS'))
        self.messages_count = int(os.environ.get('MESSAGES_COUNT', 10))

    def _stat_report(self):
        msg = ('Received total %(messages_total)d messages, '
               'failed to process %(messages_failed)d of them.')
        if self.process_file_wrapper.total:
            # we don't want to try to report file stat
            # if our implementation does not use it at all,
            # like map-data-pipeline-output-livetech
            msg += (' Got %(files_total)d files to process, '
                    'and successfully processed %(files_success)d of them.')
        logger.info(msg, dict(
            messages_total=self.process_message.total,
            messages_failed=self.process_message.fail,
            files_total=self.process_file_wrapper.total,
            files_success=self.process_file_wrapper.success,
        ))

    @collect_stats
    def process_message(self, message):
        """
        Override this method if you want to implement
        general SQS message processing,
        not S3 file handling.

        This method is responsible for properly finalizing message
        after processing, i.e. delete or release it.
        The simplest way is to wrap most code in ``with message:`` block.

        :param kasasa_lambda.sqs.Message message: message object to process
        """

        # TODO check if we want to or can process this message!
        # Filter out test events:
        # if message.data.get('Event', '') == 's3:TestEvent':
        # (or broader:)
        # if message.data.get('Event', '').endswith(':TestEvent'):
        # Test events (and maybe some others?) should be deleted immediately!
        # Rather than being handled.

        with message:
            if not isinstance(message.data, dict):
                raise ValueError('Got invalid message: %r' % message.data)
            if 'Records' not in message.data:
                if message.data.get('Event', '').endswith(':TestEvent'):
                    logger.info('Got test message, deleting')
                    return  # message will be deleted by context manager
                # XXX shall we delete it instead?
                # Because otherwise we will try to handle it again and again...
                raise ValueError('Got invalid message: %r' % message.data)

            logger.info('Start processing message %(msg)s', dict(msg=message))

            for record in message.data['Records']:
                bucket = record['s3']['bucket']['name']
                key = record['s3']['object']['key']

                if not self.can_process_file(bucket, key):
                    continue

                # only parse timestamp for the interesting records
                timestamp = dateutil_parse(record['eventTime'])

                # wrapper will handle stats itself
                self.process_file_wrapper(bucket, key, timestamp)

    def can_process_file(self, bucket, key):
        """
        Override this method if you want to skip processing some files.

        :returns: `True` to process the file (calling :meth:`process_file`),
            `False` otherwise.
        """
        return True

    @collect_stats
    def process_file_wrapper(self, bucket, key, timestamp):
        # this wrapper is for stats saving
        return self.process_file(bucket, key, timestamp)

    def process_file(self, bucket, key, timestamp):
        """
        Implement your desired file handling logic here.
        This method will be called only for those files
        for which :meth:`.can_process_file` returned True.

        You should implement either this method
        or :meth:`process_message_payload`.

        :param str bucket: bucket where the file resides
        :param str key: object's key
        :param datetime timestamp: event's timestamp decoded to `datetime`.
        """
        raise NotImplementedError

    # Below are various entry points;
    # use whichever you like.

    @classmethod
    def lambda_main_one_by_one(cls, event, context):
        """
        Main method for Lambda function:
        when called will process up to :envvar:`MESSAGES_COUNT` messages
        (defaults to 10)
        and then re-launch itself again if there are more messages.
        The purpose is to handle all available messages at the end
        while not bumping into lambda's time limit.

        .. note::

            It will process 10 *messages* per run,
            but they may contain more than 10 *files*.
            That is because every single message contains an array of Records,
            and there might (at least in theory) be more than one record
            per message.
        """
        self = cls()

        self.load_env()

        sqs = get_sqs(self.queue_url)

        last_exc = None
        for m in sqs.receive_max(self.messages_count):
            try:
                self.process_message(m)
            except Exception as e:
                logger.exception('Failed to handle message %s', m)
                last_exc = e

                if self.stop_on_errors:
                    raise

        # report stats for this func before scheduling the next call
        self._stat_report()

        if len(sqs) > 0:
            # there are some more messages;
            # invoke ourselves again (with no arguments) to handle them.
            logger.info('There are more messages remaining; '
                        'calling ourself again')
            get_lambda().invoke_async()

        if last_exc:
            # make sure Lambda knows there was a problem
            logger.error('There were errors handling some messages')
            raise last_exc

    @classmethod
    def lambda_main_gevent(cls, event, context):
        """
        Main method for Lambda function:
        when called will process up to :envvar:`MESSAGES_COUNT` messages
        in parallel using `gevent`,
        then re-launch itself again if there are more messages.
        Very similar to :meth:`lambda_main_one_by_one`
        but will try to process messages in parallel
        which might allow to increase MESSAGES_COUNT
        without bumping into limit.
        """
        # FIXME maybe move this func to separate module
        # to monkeypatch stuff before importing anything?
        import gevent
        from gevent import monkey
        monkey.patch_all()

        self = cls()
        self.load_env()

        sqs = get_sqs(self.queue_url)

        # first spawn all greenlets
        greenlets = []
        for m in sqs.receive_max(self.messages_count):
            g = gevent.spawn(self.process_message, m)
            # for logging purposes store msg data on greenlet object,
            # because there is no consistent method to access args
            g.msgdata = m.data  # for logging purposes,
            greenlets.append(g)
        # now wait for all of them to complete
        gevent.wait(greenlets)
        # and check results
        last_exc = None
        for g in greenlets:
            if g.exception:
                last_exc = g.exception
                logger.error(
                    'Processing message failed: %s', g.msgdata,
                    exc_info=g.exc_info,
                )

                if self.stop_on_errors:
                    raise

        self._stat_report()

        if len(sqs) > 0:
            # there are some more messages;
            # invoke ourselves again (with no arguments) to handle them.
            logger.info('There are more messages remaining; '
                        'calling ourself again')
            get_lambda().invoke_async()

        if last_exc:
            # make sure Lambda knows there was a problem
            logger.error('There were errors handling some messages')
            raise last_exc

    @classmethod
    def lambda_main_with_sublambdas(cls, event, context):
        """
        Fetch all available messages from the queue
        and pass them to sub-lambda instances, one instance per message.

        Unlike other versions, this one won't stop on first failed message,
        instead it will try to process all of them.
        Also here it is not possible to know
        when all messages are finished processing.

        .. envvar:: SUBLAMBDA

            Should contain name or ARN of the sublambda
            capable of handling these messages.
            It should accept event of the following schema::

                {
                    "body": "raw body of the SQS message",
                    "handle": "message receipt handle used to delete it"
                }

            The preferred implementation is :meth:`lambda_main_sublambda`.
        """
        self = cls()
        self.load_env()
        self.sublambda = os.environ['SUBLAMBDA']
        lam = get_lambda(self.sublambda)
        sqs = get_sqs(self.queue_url)

        for m in sqs.receive_all():
            lam.invoke_async({'body': m.body, 'handle': m.handle})

    @classmethod
    def lambda_main_sublambda(cls, event, context):
        """
        Main function for sublambda,
        required for :meth:`lambda_main_with_sublambdas`.
        Will expect message data in the event
        and will handle that message, raising an exception on failure.
        Otherwise it requires the same environment as the main lambda,
        except for :envvar:`QUEUE_URL`.
        """
        self = cls()
        self.load_env()

        # parse event
        msg_body = event['body']
        msg_handle = event['handle']
        # reconstruct SQS message
        sqs = get_sqs(self.queue_url)
        m = SQSMessage(sqs=sqs, msgid='unknown-msgid',
                       handle=msg_handle, body=msg_body)

        # process message (no need to try/catch as it is the only one);
        # exceptions will be logged automatically.
        self.process_message(m)

    @classmethod
    @xray_optional.within_segment('main')
    def main_standalone(cls):
        """
        Main method for standalone worker.
        Will process all available messages.
        Not to be used from within Lambda function.
        """
        self = cls()

        self.load_env()

        sqs = get_sqs(self.queue_url)

        last_exc = None
        for msg in sqs.receive_all():
            try:
                self.process_message(msg)
            except Exception as e:
                # exception is already logged by msg's __exit__
                logger.exception('Failed to handle message %s', msg)
                last_exc = e

                if self.stop_on_errors:
                    raise

        self._stat_report()

        if last_exc:
            logger.error('There were errors handling some messages')
            raise last_exc
