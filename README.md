# Common library for various lambda functions

Compiled documentation is available in `docs/_build/html/` directory.


XXX below are obsolete docs

## Params passed to Pig script

## Testing and deploying

### Preparing dev environment

After fetching the repository you will probably want to create amazon credentials file.
Credentials are taken from environment variables but can also be stored in `.env` file
in repository root. That file is automatically loaded into environment when app starts.
Credentials are taken from the following variables:
`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION`.

Also you will want to download pig tarball, or it will automatically be downloaded on first launch.
Download URL is declared in `common.py`.

Then you will want to use virtualenv. Create it as usual:

```
# for python3
python -m venv env
# or for python2
virtualenv env
# activate it
source env/bin/activate
# and then install dependencies
pip install -r requirements-text.txt
```

Requirements-text.txt includes main dependencies and also dev-specific ones.

## Testing

In order to run tests, just use `py.test` command. By default it will also measure coverage.
If you want coverage report in `html` format then use `--cov-report=html`.

## Deploying

We use GitLab CI for deployment. Its parameters are declared in `.gitlab-ci.yml` file.
When you push to the gitlab repo, it will run tests and after they successfully finish it will deploy to AWS.
